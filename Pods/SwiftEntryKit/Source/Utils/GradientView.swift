//
//  GradientView.swift
//  SwiftEntryKit
//
//  Created by Daniel Huri on 4/20/18.
//  Copyright (c) 2018 huri000@gmail.com. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    struct Style {
        let gradient: EKAttributes.BackgroundStyle.Gradient
        let displayMode: EKAttributes.DisplayMode
        
        init?(gradient: EKAttributes.BackgroundStyle.Gradient?,
              displayMode: EKAttributes.DisplayMode) {
            guard let gradient = gradient else {
                return nil
            }
            self.gradient = gradient
            self.displayMode = displayMode
        }
    }
    
    private let gradientLayer = CAGradientLayer()
    
    var style: Style? {
        didSet {
            setupColor()
        }
    }
    
    init() {
        super.init(frame: .zero)
        layer.addSublayer(gradientLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
    
    private func setupColor() {
        guard let style = style else {
            return
        }
        gradientLayer.colors = style.gradient.colors.map {
            $0.color(for: traitCollection, mode: style.displayMode).cgColor
        }
        gradientLayer.startPoint = style.gradient.startPoint
        gradientLayer.endPoint = style.gradient.endPoint
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setupColor()
    }
}



@IBDesignable
open class GradientDesignableView : UIView {
    
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.locations = [0, 1]
            gradientLayer.frame = bounds
            
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
        
    }
    
    @IBInspectable var newcornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    @IBInspectable var newborderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    @IBInspectable var newborderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var newshadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var newshadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var newshadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
}
