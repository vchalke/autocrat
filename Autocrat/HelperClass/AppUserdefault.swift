//
//  AppUserdefault.swift
//  Autocrat
//
//  Created by Ios_Team on 22/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
import UIKit

class AppUserdefault: NSObject {
    
    static let shared: AppUserdefault = { AppUserdefault() }()
    
    private let userDefault:UserDefaults = UserDefaults.standard;
    
    
    //MARK: - Manage Other info
    
    func save(object:Any, key:String) {
        userDefault.setValue(object, forKeyPath: key);
        userDefault.synchronize();
    }
    
    func get(key:String) -> Any? {
        return userDefault.object(forKey: key)
    }
    
    func delete(key:String) {
        userDefault.removeObject(forKey: key)
        userDefault.synchronize();
    }

    func saveStructValueForKey<T:Codable>(key:String, value:T){
        userDefault.set(try? PropertyListEncoder().encode(value), forKey:key)
    }
    
    func getStructValueForKey<T:Codable>(key:String) -> T? {
        if let data = userDefault.value(forKey:key) as? Data {
            let userDetail = try? PropertyListDecoder().decode(T.self, from: data)
            return userDetail
        }
        return nil
    }
    func saveBoolValueForKey(key:String, value:Bool){
        userDefault.set(value, forKey: key)
    }
    
    func getBoolValueForKey(key:String) -> Bool? {
        return userDefault.value(forKey: key) as? Bool
    }
}
