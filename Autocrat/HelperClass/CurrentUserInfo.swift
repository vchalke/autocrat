//
//  CurrentUserInfo.swift
//  Autocrat
//
//  Created by Ios_Team on 22/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation

class CurrentUserInfo {
    
    static func insertUserInfoIntoDefaults(model:UserModel) {
        self.userId = model.user_id
        self.fullName = model.fullName
        self.userName = model.userName
        self.email = model.email
        self.password = model.password
        self.img = model.img
        self.accessToken = model.accessToken
        self.token = model.token
        self.isLogin = true
    }
    
    static func clearDefaults() {
        self.userId = nil
        self.fullName = nil
        self.userName = nil
        self.email = nil
        self.password = nil
        self.accessToken = nil
        self.token = nil
        self.img = nil
    }
    
    static var userId:Int? {
        get {
            return UserDefaults.standard.integer(forKey: "userId")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "userId")
        }
    }
    
    static var fullName:String? {
        get {
            return UserDefaults.standard.value(forKey: "fullName") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "fullName")
        }
    }
    
    static var userName:String? {
        get {
            return UserDefaults.standard.value(forKey: "userName") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "userName")
        }
    }
    
    static var email:String? {
        get {
            return UserDefaults.standard.value(forKey: "email") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "email")
        }
    }
    
    static var password:String? {
        get {
            return UserDefaults.standard.value(forKey: "password") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "password")
        }
    }
    
    static var accessToken:String? {
        get {
            return UserDefaults.standard.value(forKey: "accessToken") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "accessToken")
        }
    }
    static var token:String? {
        get {
            return UserDefaults.standard.value(forKey: "token") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "token")
        }
    }
    static var isLogin:Bool {
        get {
            return UserDefaults.standard.bool(forKey: "islogin")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "islogin")
        }
    }
    
    static var img:String? {
        get {
            return UserDefaults.standard.value(forKey: "img") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "img")
        }
    }
    
    static var state_code:String? {
        get {
            return UserDefaults.standard.value(forKey: "state_code") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "state_code")
        }
    }
}



public enum UserDefaultsConstant:String {
    case USER_ID = "userId"
    case FULL_NAME = "fullName"
    case USER_NAME = "userName"
    case EMAIL = "email"
    case PASS_WORD = "password"
    case ACCESS_TOKEN = "accessToken"
    case TOKEN = "token"
    case IS_LOGIN = "islogin"
    case IMG = "img"
    case STATE_CODE = "state_code"
}
