//
//  MessageConstant.swift
//  Autocrat
//
//  Created by Ios_Team on 22/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
public enum MessagesSuccess : String {
    case LOGIN_SUCCESS = "Logged In Successfully"
    case LOGOUT = "Are you sure you want to logout?"
}

public enum MessageEmpty: String {
    case EMPTY_USERNAME   = "Username field can not be blank."
    case EMPTY_FIRST_NAME = "Please enter first name."
    case EMPTY_LAST_NAME = "Please enter last name."
    case EMPTY_NAME = "Please enter name."
    case EMPTY_EMAIL = "Please enter email address."
    case EMPTY_PHONE = "Please enter mobile number."
    case EMPTY_PASSWORD = "Password must be at least 8 characters and contain 1 letter, 1 number, at least 1 uppercase and at least 1 lowercase"
    case EMPTY_OLD_PASSWORD = "Old password field can not be blank."
    case EMPTY_NEW_PASSWORD = "New password field can not be blank."
    case EMPTY_CONFIRM_PASSWORD = "Confirm password field can not be blank."
}
public enum MessageError: String {
    case INTERNET_ERROR = "Please check your internet connection"
    case DIFFERENT_PASSWORD = "New password did not match with confirm password."
    case LENGTH_OF_PASSWORD = "Password must be equal or greater than 6 digit."
}
public enum MessageInvalid: String {
    case INVALID_EMAIL = "Please enter correct email address."
    case INVALID_PHONE = "Please enter correct mobile number."
}
public enum AlertButton: String {
    case OK     = "OK"
    case CANCEL = "Cancel"
    case YES    = "Yes"
    case NO     = "No"
    case ALLOW     = "Allow"
    case PEOCEED     = "Proceed"
}
