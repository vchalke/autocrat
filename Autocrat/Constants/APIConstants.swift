//
//  APIConstants.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation

// --------- Web services ------------
let kServiceUrl       = "http://172.105.60.14/public/api/"
let baseFilePath      = "/Users/os/Desktop/Company_Project/ClientApp/Autocrat/Autocrat/CSVFiles/"

// --------- Titles ------------
public enum MethodName:String {
    case LOGIN_WITH_ANY = "loginwithany"
    case LOGIN = "login"
    case REGISTER = "signup"
    case SOCIAL_LOGIN = "social_login"
    case SOCIAL_LOGIN_FB = "social_login_fb"
    case SOCIAL_LOGIN_GMAIL = "social_login_gmail"
    case SOCIAL_LOGIN_CHECK = "social_login_check"
    case LOGOUT = "logout"
    case VERIFY_REGISTER_OTP = "verify_otp"
    case RESEND_REG_OTP = "resend_otp"
    case FORGOT_PASS = "forgot_password"

}
public enum FileName:String {
    case DEALERSHIP_BID_OFFER = "DealershipBidOffer"
    case EMPLOYMENT_STATUS_COPY = "EmploymentStatus"
    case PROPERTY_SERVICE_MARITIAL = "PropertyServiceMaritial"
    case ADDRESS_LISCENCE = "AddressLiscence"
    case LOAN_INFO = "LoanInfo"
}




let dateFormat        = "yyyy-MM-dd, HH:mm:ss.SSSZ"
let dateFormatBackend = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
let dateFormatBackOth = "yyyy-MM-dd'T'HH:mm:ss+00:00"
