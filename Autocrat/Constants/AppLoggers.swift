//
//  AppLoggers.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
import UIKit

struct AppLoggerConstant {
    static let isLogEnable: Bool = true
}


public func printLog(_ items: Any...)  {
    if AppLoggerConstant.isLogEnable {
        if  items.count > 0
        {
            for item in items {
                print(item)
            }
        }
    }
}
