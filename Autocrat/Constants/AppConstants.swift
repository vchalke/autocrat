//
//  AppConstants.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
import UIKit
let kAppName = "CanDo"
var kWindow = UIWindow(frame: UIScreen.main.bounds)
let kApplicationDelegate = UIApplication.shared.delegate as! AppDelegate
var kScreenSize = UIScreen.main.bounds.size
let kUserDefault = UserDefaults.standard
let kFrameWidth = UIScreen.main.bounds.size.width
let kFrameHeight = UIScreen.main.bounds.size.height

let kSignIn             = "SIGNIN"
let kSignUP             = "SIGNUP"
let kForgotPassword     = "FORGOTPASSWORD"
let kUserDetailInfo     = "USERDETAILSINFO"

// Titles
let kSignInTitle            = "Sign In"
let kSignUPTitle           = "Sign Up"
let kForgotPasswordTitle    = "Forgot Password"

let kIsFromHomeVC = "isFromHome"
