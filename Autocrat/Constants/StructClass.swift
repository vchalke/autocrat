//
//  StructClass.swift
//  Autocrat
//
//  Created by Ios_Team on 07/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
import UIKit

struct User_Details:Codable {
    var user_id: String
    var email_id: String
    var first_Name: String
    var last_Name: String
    var full_Name: String
    var img_Url: URL
}

//var userDetails: User_Details = [
//    User_Details(user_id: <#T##String#>, email_id: <#T##String#>, first_Name: <#T##String#>, last_Name: <#T##String#>, full_Name: <#T##String#>)
//]
