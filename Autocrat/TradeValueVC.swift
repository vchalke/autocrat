//
//  TradeValueVC.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class TradeValueVC: MasterVC {

    @IBOutlet weak var lbl_Message: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lbl_Message.text = "1. Instant online quote\n2. Free doorstep inspection\n3. Same day payment"
    }


    
    @IBAction func btnLiscenePressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEnterPinPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
}
