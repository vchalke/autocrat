//
//  APILoginModule.swift
//  Autocrat
//
//  Created by Ios_Team on 22/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation

class APILoginModule: NSObject {
    static func emailLogin(by urlString : String , para : Data, httpMeethodType : String){
        
        let request = HttpHeader.shared.initWithPostHeader(urlString: urlString, Contentype: HeaderName.urlencode.rawValue,Accept: HeaderName.Json.rawValue,Json: para)
        Services.shared.initWithURlDecoable(request: request, decodingType: successKey<UserModel>.self) { (results) in
            printLog(results)
            
            switch results
            {
            case .success(let model , _) :
                let modelLogin:UserModel = model.success!
                let deadlineTime = DispatchTime.now() + .seconds(2)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    NotificationCenters.postNotificationWithIdentifier(identifier: NotificationCentersConstant.SUCCESS.rawValue, userInfo: getDictionaryFromModel(model: modelLogin) ?? ["SUCCESS":"SUCCESS"])
                }
                break
            case .failureInServer(let dict, _, _):
                let deadlineTime = DispatchTime.now() + .seconds(2)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    NotificationCenters.postNotificationWithIdentifier(identifier: NotificationCentersConstant.FAILURE_IN_SERVER.rawValue, userInfo: dict)
                }
                break
            case .failure( _):
                let deadlineTime = DispatchTime.now() + .seconds(2)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    NotificationCenters.postNotificationWithIdentifier(identifier: NotificationCentersConstant.FAILURE_IN_API.rawValue, userInfo: ["failure":NotificationCentersConstant.FAILURE_IN_API.rawValue])
                }
            }
        }
    }
    
    
    static func getDictionaryFromModel(model:UserModel) -> [String :Any]? {
        if let JsonObject = try?  JSONEncoder().encode(model){
            if let dictionary = try? JSONSerialization.jsonObject(with: JsonObject, options: .allowFragments) as? [String:Any]{
                return dictionary
            }
        }
        return nil
    }
    
}
