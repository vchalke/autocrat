//
//  HTTPHeader.swift
//  Autocrat
//
//  Created by Ios_Team on 22/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation

public enum HeaderName:String {
    case Json = "application/json"
    case Multiform = "multipart/form-data"
    case auth = "Authorization"
    case urlencode = "application/x-www-form-urlencoded"

}

public enum MethodType:String {
    case GET = "GET"
    case POST = "POST"
    case DELETE = "DELETE"
    case PUT = "PUT"

}

public enum ResponseType:String {
    case SUCCESS = "Success"
    case FAILURE_IN_SERVER = "FailureInServer"
    case FAILURE_IN_API = "FailureInApi"
    case REQUEST_FAILED = "Request Failed"
    case INVALID_DATA = "Invalid Data"
    case JSON_PARSING_FAILURE = "JSON Parsing Failure"
    case JSON_CONVERSION_FAILURE = "JSON Conversion Failure"
    case INTERNET_GONE = "Internet Gone"

}

public enum HeaderField:String {
    case CONTENT_TYPE = "Content-Type"
    case ACCEPT = "Accept"

}


class HttpHeader {
    static let shared = {return HttpHeader()}()
    
    func initWithPostHeader(urlString : String,Contentype :String? = nil ,Accept :String? = nil, para:[String:Any]? = nil, Json:Data? = nil) -> URLRequest {
        let serviceUrl : URL = URL(string: urlString)!
        var request = URLRequest(url:serviceUrl)
        if let contentType  = Contentype {
            request.addValue(contentType, forHTTPHeaderField: HeaderField.CONTENT_TYPE.rawValue)
        }
        if let accept = Accept {
            request.addValue(accept, forHTTPHeaderField: HeaderField.ACCEPT.rawValue)
        }
        if let jsonData = Json {
            request.httpBody = jsonData
        }
        if let data = para {
            request.httpBody = self.getHTTPBody(para: data)
        }
        if (CurrentUserInfo.isLogin ){
            request.setValue("Bearer " + CurrentUserInfo.token! , forHTTPHeaderField: "Authorization")
        }
        request.httpMethod = MethodType.POST.rawValue
        return request
    }
    
    private func getHTTPBody(para:[String:Any]?) -> Data
    {
        return try! JSONSerialization.data(withJSONObject: para!, options: [])
    }
    
    func initWithGetHeader(urlString : String,Contentype :String? = nil ,Accept :String? = nil,para:[String:String]? = [:]) -> URLRequest {
        
        let serviceUrl = createURlWithParamterInit(urlString: urlString, para: para!)
        var request = URLRequest(url:serviceUrl)
        if let contentType  = Contentype {
            request.setValue(contentType, forHTTPHeaderField: HeaderField.CONTENT_TYPE.rawValue)
        }
        if let accept = Accept {
            request.setValue(accept, forHTTPHeaderField: HeaderField.ACCEPT.rawValue)
        }
        if (CurrentUserInfo.isLogin ){
            request.setValue("Bearer " + CurrentUserInfo.token!, forHTTPHeaderField: "Authorization")
        }
        request.httpMethod = MethodType.GET.rawValue
        return request
        
    }
    
    func initHeaderWithPostHeaderWithMultipart(urlString : String,Contentype :String? = nil ,Accept :String, para:Data? = nil ,paras : [String : Any]? = nil,fileName : String, MemeData : Data?) -> URLRequest
    {
        let serviceUrl : URL = URL(string: urlString)!
        var request = URLRequest(url:serviceUrl)
        request.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("\(Accept)", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(CurrentUserInfo.token!)", forHTTPHeaderField: "Authorization")
        
        
        let body = NSMutableData()
//        let fname = fileName
//        let mimetype = "image/png"
      
        if let songRawData = MemeData
        {
            body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"image\"; filename=\"file.png\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                   body.append(songRawData)
        }
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        if let datas = paras {
            for (key, value) in datas {
                    body.append("\r\n--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
                }
                body.append("\r\n--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)

            }
        request.httpBody = body as Data
        return request
        
    }
    func initAlamoWithPostHeaderWithMultipart(paras : [String : Any]? = nil){
        
    }
    private func createURlWithParamterInit(urlString : String, para:[String:String]) -> URL {
        var item = [URLQueryItem]()
        var myUrl = URLComponents(string: urlString)
        for (key,value) in para
        {
            item.append(URLQueryItem(name: key, value: value ))
        }
        myUrl?.queryItems = item
        if let url = myUrl?.url {
            return url}
        else
        {return URL(string: urlString)!}
    }
    
}
