//
//  RequestResponse.swift
//  Autocrat
//
//  Created by Ios_Team on 22/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation

import UIKit

public class RequestResponse: Codable {
    
    public var status               :Bool?
    public var data                 :AnyDecodable?
    public var message              :String?
    public var code                 :Int?
    public var responseDict         :[String:Any]?
    //public var errors             :String?
    
    public func isSuccess() -> Bool {
        if status == true {
            return true;
        }
        return false;
    }
    
    enum CodingKeys: String, CodingKey {
        case status
        case data
        case message
        case code
        //case errors
    }
    
    public func encode(to encoder: Encoder) throws {
        
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        data = try values.decodeIfPresent(AnyDecodable.self, forKey: .data)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
    }
    
    init(flag:Bool,strMessage:String) {
        self.status = flag
        self.message = strMessage
    }
    
    init(dataDict:[String:Any],errorType:String) {
        self.responseDict = dataDict
        self.message = errorType
    }
}
