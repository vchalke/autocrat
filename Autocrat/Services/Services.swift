//
//  Services.swift
//  Autocrat
//
//  Created by Ios_Team on 22/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
enum Result<T, U> {
    case success(T,Int)
    case failureInServer([String : Any],U,Int)
    case failure(U)
}

struct successKey<T> : Decodable where T : Decodable{
    var success : T?
}


class CommonParam: NSObject {
    
    static func setHeader() -> [String : String] {
        if let accessToken = CurrentUserInfo.accessToken {
            let dic = [
                "Accept": "application/json",
                "Authorization" : accessToken
            ];
            return dic
        }
        return [:]
    }
    
    static func getDefaultParams(params:[String:Any])-> [String:Any]{
        var _params = params;
        _params["device_type"] = "ios"
        _params["device_token"] = "tokens"
        return _params;
    }
    
    static func jsonData(obj:Any) -> Data{
        let jsonData = try? JSONSerialization.data(withJSONObject: obj, options: [])
        let reqJSONStr = String(data: jsonData ?? Data(), encoding: .utf8)
        let data = reqJSONStr?.data(using: .utf8)
        
        return data!
    }
    
}


/*
class Services{
    
    static let isDebug = true;
    
    static func getErrorMessage(_ message:String = MessageError.INTERNET_ERROR.rawValue) -> RequestResponse{
        return RequestResponse(flag: false, strMessage: MessageError.INTERNET_ERROR.rawValue)
    }
    
    static func getRequestWithParams(url: String,_ params:[String : Any], callback: @escaping (RequestResponse) -> Void) {
        //        let _params = Common.getDefaultParams(params: params)
        printLog([url, params])
        print(CommonParam.setHeader())
        if NetworkManager.isConnectedToNetwork() {
            Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: CommonParam.setHeader()).responseJSON(completionHandler: { (response) in
                printLog(response)
                completion(result: response.result, data: response.data, callback: callback);
            })
        }else{
            callback(getErrorMessage());
        }
    }
    
    static func getRequest(url: String, callback: @escaping (RequestResponse) -> Void) {
        
        print("Base URL :- ", url);
        print("Header :- ", CommonParam.setHeader());
        if NetworkManager.isConnectedToNetwork() {
            Alamofire.request(url, method: .get,encoding: JSONEncoding.default, headers: CommonParam.setHeader()).responseJSON(completionHandler: { (response) in
                printLog(response)
                 LoadingView.hide()
                completion(result: response.result, data: response.data, callback: callback);
            })
            
        }else{
            callback(getErrorMessage());
        }
    }
    
    static func postRequest(url: String,_ params: [String : Any], method:HTTPMethod = .post, callback: @escaping (RequestResponse) -> Void) {
        let _params =  params
        printLog(url, _params)
        if NetworkManager.isConnectedToNetwork() {
            Alamofire.request(url, method: method, parameters: _params, encoding: JSONEncoding.default, headers:CommonParam.setHeader()).responseJSON(completionHandler: { (response) in
                printLog(response)
                if let value = response.result.value as? [String: Any]{
                    if let code:Int = value["code"] as? Int , code == 401 {
                        LoadingView.hide()
                    }
                }
                
                completion(result: response.result, data: response.data, callback: callback);
            })
            
        }else{
            callback(getErrorMessage());
        }
    }
    
    
    static func completion(result: Result<Any>, data:Data?, callback: (RequestResponse) -> Void)  {
        if(result.isSuccess) {
            
            if let value = result.value as? [String: Any]{
                if let _ = value["status"] {
                    let decoder = JSONDecoder()
                    do {
                        let data = try JSONSerialization.data(withJSONObject: result.value as Any, options: .prettyPrinted)
                        let object = try! decoder.decode(RequestResponse.self, from: data)
                        callback(object)
                    } catch {
                        callback(RequestResponse(flag: false, strMessage: "Something went wrong,Please try again."));
                    }
                } else {
                    callback(RequestResponse(flag: false, strMessage: value["message"] as? String ?? ""))
                }
            }else{
                let value = result.value
                callback(value as! RequestResponse)
            }
        } else {
            callback(RequestResponse(flag: false, strMessage: "No server response"));
        }
    }
    
}
*/



class Services {
    static let shared = {return Services()}()
     func getSession() -> URLSession
    {
        let config =  URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30.0
        config.timeoutIntervalForResource = 60.0
        return URLSession(configuration: config)
    }
    
    func initWithURlDecoable<T:Decodable>(request : URLRequest,decodingType: T.Type,completion :@escaping (Result<T,String>)->()) {
        let session = getSession()
        var datatask : URLSessionDataTask?
        datatask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil {
//                completion(Result.failure(ResponseType.REQUEST_FAILED.rawValue))
                completion(Result.failure(ResponseType.REQUEST_FAILED.rawValue))
                return
            }
            guard let httpResponses = response as? HTTPURLResponse else {return}
            let statusCode = httpResponses.statusCode
            if let ObjData = data {
                print(ObjData.getString())
                do{
                    
                    if(statusCode >= 200 && statusCode <= 300){
                        let jsonData = try JSONDecoder().decode(T.self, from: ObjData)
                        completion(Result.success(jsonData, httpResponses.statusCode))
                    }
                    else{
                        let dictionary = try JSONSerialization.jsonObject(with: ObjData, options: .allowFragments) as? [String:Any]
                        completion(Result.failureInServer(dictionary!, ResponseType.INVALID_DATA.rawValue,statusCode))
                    }
                    return
                }
                catch let err {
                    print(err)
                    completion(Result.failure(ResponseType.JSON_CONVERSION_FAILURE.rawValue))
                    return
                }
            }
        })
        datatask?.resume()
    }
    
    
    func initWithURlDictionary(request :URLRequest,completion :@escaping (RequestResponse)->()) {
        
        let session = getSession()
        var datatask : URLSessionDataTask?
        datatask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let _ = error  {
                completion(RequestResponse(dataDict: [:], errorType: ResponseType.REQUEST_FAILED.rawValue));
            }
            guard let httpResponses = response as? HTTPURLResponse else {return}
            let statusCode = httpResponses.statusCode
            if let ObjData = data {
                do{
                    print(ObjData.getString())
                    if let dictionary = try JSONSerialization.jsonObject(with: ObjData, options: .allowFragments) as? [String:Any]{
                        if(statusCode >= 200 && statusCode <= 300){
                            completion(RequestResponse(dataDict: dictionary, errorType: ResponseType.SUCCESS.rawValue));
                        }
                        else
                        {
                            completion(RequestResponse(dataDict: dictionary, errorType: ResponseType.FAILURE_IN_SERVER.rawValue));
                        }
                    }
                }
                catch let err{
                    print(err.localizedDescription)
                    completion(RequestResponse(dataDict: [:], errorType: ResponseType.FAILURE_IN_API.rawValue));
                }
            }
        })
        datatask?.resume()
    }
func sendStatusCodeError(_ statusCode  :Int)  {
    NotificationCenter.default.post(name: Notification.Name("StatusError"),object :statusCode)
    
}
func obeserverinStatusError(in Vc : UIViewController , completion : @escaping (Int) -> Void) {
    NotificationCenter.default.addObserver(forName: NSNotification.Name("StatusError"), object: nil, queue: nil) { (notification) in
        completion((notification.object as? Int)!)
    }
}
func sstopObeservering(in vc :UIViewController) {
    NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("StatusError"), object: nil)
}

}


extension Data
{
    func getString() -> String
    {
        return String(data: self, encoding: .utf8)!
    }
}
