//
//  NotificationCenters.swift
//  Autocrat
//
//  Created by Ios_Team on 23/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
class NotificationCenters: NSObject {
    static func postNotificationWithIdentifier(identifier:String,userInfo:[String:Any]){
        NotificationCenter.default.post(name: Notification.Name(identifier), object: nil, userInfo: userInfo)
    }
    
}


public enum NotificationCentersConstant:String {
    case SUCCESS = "SUCCESS"
    case FAILURE_IN_SERVER = "FAILURE_IN_SERVER"
    case FAILURE_IN_API = "FAILURE_IN_API"
}
