//
//  IBDesignable.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class DessignableViewButton : UIButton {
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            //            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            //            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    private func setGradient(topGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            //            gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            self.gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
        
    }
    
    @IBInspectable var newcornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    @IBInspectable var newborderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    //    @IBInspectable var newborderColor: UIColor? {
    //         get {
    //            return UIColor(cgColor: self.layer.borderColor!)
    //         }
    //         set {
    //            self.layer.borderColor = newValue?.cgColor
    //         }
    //    }
    @IBInspectable var newshadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var newshadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var newshadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
    
}
@IBDesignable
class DessignableImage : UIImageView {
}
@IBDesignable
class DessignableView : UIView {
}
@IBDesignable
class DessignableTextField : UITextField, UITextFieldDelegate {
    
    @IBInspectable var leftImage : UIImage? {
        didSet {
            updateTextField(withLeft: true)
        }
    }
    
    @IBInspectable var leftPadding : CGFloat = 0 {
        didSet {
            updateTextField(withLeft: true)
        }
    }
    
    @IBInspectable var cornerRadiusOfField : CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadiusOfField
        }
    }
    
    /// Sets the color of the border
    @IBInspectable public override var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable public var underLineWidth: CGFloat = 0.0 {
        didSet {
            updateUnderLineFrame()
        }
    }
    
    /// Sets the underline color
    @IBInspectable public var underLineColor: UIColor = .groupTableViewBackground {
        didSet {
            updateUnderLineUI()
        }
    }
    
    /// Sets the placeholder color
    @IBInspectable public var placeholderColor: UIColor = .lightGray {
        didSet {
            let placeholderStr = placeholder ?? ""
            attributedPlaceholder = NSAttributedString(string: placeholderStr, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
    
    
    
    // MARK: - Underline
    private var underLineLayer = CALayer()
    private func applyUnderLine() {
        // Apply underline only if the text view's has no borders
        if borderStyle == UITextField.BorderStyle.none {
            underLineLayer.removeFromSuperlayer()
            updateUnderLineFrame()
            updateUnderLineUI()
            layer.addSublayer(underLineLayer)
            layer.masksToBounds = true
        }
    }
    
    private func updateUnderLineFrame() {
        var rect = bounds
        rect.origin.y = bounds.height - underLineWidth
        rect.size.height = underLineWidth
        underLineLayer.frame = rect
    }
    
    private func updateUnderLineUI() {
        underLineLayer.backgroundColor = underLineColor.cgColor
    }
    
    func updateTextField(withLeft:Bool) {
        if let image = leftImage {
            rightViewMode = .always
            var width = leftPadding + 20
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            let imageView = UIImageView(frame: CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: 20, height: 20))
            imageView.image = image
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view.addSubview(imageView)
            rightView = view
        } else {
            rightViewMode = .never
        }
    }
    
    // MARK: - init methods
    override public init(frame: CGRect) {
        super.init(frame: frame)
         delegate = self
        applyStyles()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         delegate = self
        applyStyles()
    }
    
    // MARK: - Layout
    override public func layoutSubviews() {
        super.layoutSubviews()
        updateUnderLineFrame()
    }
    
    // MARK: - Styles
    private func applyStyles() {
        applyUnderLine()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearButtonMode = .whileEditing
       
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}
/*
@IBDesignable
class RightAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        semanticContentAttribute = .forceRightToLeft
        contentHorizontalAlignment = .center
        //        titleLabel?.textAlignment = .left
        let availableSpace = bounds.inset(by: contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2)
    }
}
@IBDesignable
class LeftAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height*0.5
        layer.borderWidth = 0.8
        layer.borderColor = UIColor.gray as! CGColor
        contentHorizontalAlignment = .left
        let availableSpace = bounds.inset(by: contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.right - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: availableWidth / 2, bottom: 0, right: 0)
    }
}
*/
@IBDesignable
class LeftAlignedIconButton: UIButton {
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let titleRect = super.titleRect(forContentRect: contentRect)
        let imageSize = currentImage?.size ?? .zero
        let availableWidth = contentRect.width - imageEdgeInsets.right - imageSize.width - titleRect.width
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: center.x/2 , bottom: 0.0, right: 0.0)
        return titleRect.offsetBy(dx: round(availableWidth / 2), dy: 0)
    }
}

@IBDesignable
class RightAlignedIconButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        semanticContentAttribute = .forceRightToLeft
        contentHorizontalAlignment = .right
        let availableSpace = bounds.inset(by: contentEdgeInsets)
        let availableWidth = availableSpace.width - imageEdgeInsets.left - (imageView?.frame.width ?? 0) - (titleLabel?.frame.width ?? 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: availableWidth / 2)
    }
}


@IBDesignable
class SpacingTextField: UITextField {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.delegate = self
        keyboardType = .numberPad
        //        let underlinewidh = bounds.width/11.8
        //        let dashLength = underlinewidh+(underlinewidh*0.5)
        //        let spaceLength = underlinewidh*0.4
        //        let space = spaceLength + (dashLength*0.5)
        //        self.defaultTextAttributes.updateValue(space, forKey: NSAttributedString.Key.kern)
        self.addTarget(self, action: #selector(textFieldDidChange),for: .editingChanged)
        
    }
    @IBInspectable var leftPadding : CGFloat = 0 {
        didSet {
            let dashSize = bounds.width/11.8
            setLeftPaddingPoints(dashSize*0.4)
        }
    }
   func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    @IBInspectable var maxLength : Int = 36 {
        didSet {
            limitCharacters()
        }
    }
    func limitCharacters() {
        //        guard text != nil else {
        //            return
        //        }
        //        if (text?.count ?? 0) > maxLength {
        //            if let range = text?.index(before: (text?.endIndex)!) {
        //                text = text?.substring(to: range)
        //            }
        //        }
    }
    @objc func textFieldDidChange () {
        print(text?.count ?? 100)
        print(maxLength)
        print("TextField Text : \(text ?? "")")
        let underlinewidh = bounds.width/11.8
        let dashLength = underlinewidh+(underlinewidh*0.5)
        let spaceLength = underlinewidh*0.4
        let space = spaceLength + (dashLength*0.52)
        
        if((text?.count ?? 0) >= maxLength)
        {
            self.resignFirstResponder()
            return
        }
        let attributedString = NSMutableAttributedString(string: self.text ?? "")
        attributedString.addAttribute(NSAttributedString.Key.kern, value: space, range: NSMakeRange(0, text?.count ?? 0))
        // attributedString.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, count(textField.text)))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, text?.count ?? 0))
        self.attributedText = attributedString
        
    }
}

extension SpacingTextField: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if text?.count ?? 0 < maxLength {
            return true
//            // Else if 4 and delete is allowed
//        }else if text?.count ?? 0 == maxLength {
//            textField.resignFirstResponder()
//            return true
//            // Else limit reached
//        }else{
//            return false
//        }
    }
    
}
@IBDesignable
class DashedLineView : UIView {
    @IBInspectable var perDashLength: CGFloat = 2.0
    @IBInspectable var spaceBetweenDash: CGFloat = 2.0
    @IBInspectable var dashColor: UIColor = UIColor.lightGray
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let  path = UIBezierPath()
        if height > width {
            let  p0 = CGPoint(x: self.bounds.midX, y: self.bounds.minY)
            path.move(to: p0)
            
            let  p1 = CGPoint(x: self.bounds.midX, y: self.bounds.maxY)
            path.addLine(to: p1)
            path.lineWidth = width
            
        } else {
            let  p0 = CGPoint(x: self.bounds.minX, y: self.bounds.midY)
            path.move(to: p0)
            
            let  p1 = CGPoint(x: self.bounds.maxX, y: self.bounds.midY)
            path.addLine(to: p1)
            path.lineWidth = height
        }
        let underlinewidh = (self.bounds.maxX)/7
        perDashLength = underlinewidh
        spaceBetweenDash = underlinewidh
        let  dashes: [ CGFloat ] = [ perDashLength, spaceBetweenDash ]
        path.setLineDash(dashes, count: dashes.count, phase: 0.0)
        
        path.lineCapStyle = .butt
        dashColor.set()
        path.stroke()
    }
    
    private var width : CGFloat {
        return self.bounds.width
    }
    
    private var height : CGFloat {
        return self.bounds.height
    }
}
@IBDesignable
class DessignableLabel : UILabel {
    
    @IBInspectable override var text : String? {
        didSet {
            decorate()
        }
    }
    
    @IBInspectable var fontSize : CFloat = 12.0 {
        didSet {
            decorate()
        }
    }
    
    
    @IBInspectable var fontColor : UIColor? {
        didSet {
            decorate()
        }
    }
    
    func decorate() {
        
        guard let text = text else { return }
        
        let titleAttributess = [
            NSAttributedString.Key.font.rawValue : UIFont.systemFont(ofSize: CGFloat(fontSize)),
            NSAttributedString.Key.foregroundColor : fontColor ?? UIColor.black,
            NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue
            ] as! [NSAttributedString.Key : Any]
        
        let titleString = NSMutableAttributedString(string: text, attributes: titleAttributess)
        
        // Other attributes
        titleString.addAttribute(NSAttributedString.Key.kern, value: 0, range: NSRange(location: 0, length: text.count))
        
        self.attributedText = titleString
    }
}


extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            //            self.layer.masksToBounds = true
        }
    }
    @IBInspectable
    var roundArea : Bool
    {
        get {
            return true
        }
        set
        {
            self.layer.cornerRadius = self.frame.size.width / 2
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
}
@IBDesignable
class GradientView: UITableView {
    
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map {$0.cgColor}
        if (isHorizontal) {
            layer.startPoint = CGPoint(x: 0, y: 0.5)
            layer.endPoint = CGPoint (x: 1, y: 0.5)
        } else {
            layer.startPoint = CGPoint(x: 0.5, y: 0)
            layer.endPoint = CGPoint (x: 0.5, y: 1)
        }
    }
    
}
@IBDesignable
class MovingPlaceHolderTextField: UITextField, UITextFieldDelegate{

    let border = CALayer()

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        delegate = self
        createBorder()
        setup()
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        delegate = self
        createBorder()
        setup()
    }

    func createBorder(){
        let width = CGFloat(2.0)
        border.borderColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearButtonMode = .whileEditing
        movePlaceholderUp()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        movePlaceholderDown()
    }

    func movePlaceholderUp(){
        border.borderColor = UIColor.colorWithHexString(hex: "004E9B").cgColor
    }
    func movePlaceholderDown(){
        border.borderColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0).cgColor
    }
    let animationDuration = 0.3
    var title = UILabel()
    
    // MARK:- Properties
    override var accessibilityLabel:String? {
        get {
            if let txt = text , txt.isEmpty {
                return title.text
            } else {
                return text
            }
        }
        set {
            self.accessibilityLabel = newValue
        }
    }
    
    override var placeholder:String? {
        didSet {
            title.text = placeholder
            title.sizeToFit()
        }
    }
    
    override var attributedPlaceholder:NSAttributedString? {
        didSet {
            title.text = attributedPlaceholder?.string
            title.sizeToFit()
        }
    }
    
    var titleFont:UIFont = UIFont.systemFont(ofSize: 12.0) {
        didSet {
            title.font = titleFont
            title.sizeToFit()
        }
    }
    
    @IBInspectable var hintYPadding:CGFloat = 0.0{
        didSet {
            var r = title.frame
            r.origin.y = titleYPadding
            title.frame = r
        }
    }

    @IBInspectable var titleYPadding:CGFloat = 0.0 {
        didSet {
            var r = title.frame
            r.origin.y = titleYPadding
            title.frame = r
        }
    }
    
    @IBInspectable var titleTextColour:UIColor = UIColor.gray {
        didSet {
            if !isFirstResponder {
                title.textColor = titleTextColour
            }
        }
    }
    
    @IBInspectable var titleActiveTextColour:UIColor! {
        didSet {
            if isFirstResponder {
                title.textColor = titleActiveTextColour
            }
        }
    }
        
    // MARK:- Init
//    required init?(coder aDecoder:NSCoder) {
//        super.init(coder:aDecoder)
//        setup()
//    }
//
//    override init(frame:CGRect) {
//        super.init(frame:frame)
//        setup()
//    }
    
    // MARK:- Overrides
    override func layoutSubviews() {
        super.layoutSubviews()
        setTitlePositionForTextAlignment()
        let isResp = isFirstResponder
        if let txt = text , !txt.isEmpty && isResp {
            title.textColor = titleActiveTextColour
        } else {
            title.textColor = titleTextColour
        }
        // Should we show or hide the title label?
        if let txt = text , txt.isEmpty {
            // Hide
            hideTitle(isResp)
        } else {
            // Show
            showTitle(isResp)
        }
    }
    
    override func textRect(forBounds bounds:CGRect) -> CGRect {
        var r = super.textRect(forBounds: bounds)
        if let txt = text , !txt.isEmpty {
            var top = ceil(title.font.lineHeight + hintYPadding)
            top = min(top, maxTopInset())
            r = r.inset(by: UIEdgeInsets.init(top: top, left: 0.0, bottom: 0.0, right: 0.0))
        }
        return r.integral
    }
    
    override func editingRect(forBounds bounds:CGRect) -> CGRect {
        var r = super.editingRect(forBounds: bounds)
        if let txt = text , !txt.isEmpty {
            var top = ceil(title.font.lineHeight + hintYPadding)
            top = min(top, maxTopInset())
            r = r.inset(by: UIEdgeInsets.init(top: 0, left: 0.0, bottom: 0.0, right: 0.0))
        }
        return r.integral
    }
    
    override func clearButtonRect(forBounds bounds:CGRect) -> CGRect {
        var r = super.clearButtonRect(forBounds: bounds)
        if let txt = text , !txt.isEmpty {
            var top = ceil(title.font.lineHeight + hintYPadding)
            top = min(top, maxTopInset())
            r = CGRect(x:r.origin.x, y:r.origin.y + (top * 0.5), width:r.size.width, height:r.size.height)
        }
        return r.integral
    }
    
    // MARK:- Public Methods
    
    // MARK:- Private Methods
    fileprivate func setup() {
        borderStyle = UITextField.BorderStyle.none
        titleActiveTextColour = tintColor
        // Set up title label
        title.alpha = 0.0
        title.font = titleFont
        title.textColor = titleTextColour
        if let str = placeholder , !str.isEmpty {
            title.text = str
            title.sizeToFit()
        }
        self.addSubview(title)
    }

    fileprivate func maxTopInset()->CGFloat {
        if let fnt = font {
            return max(0, floor(bounds.size.height - fnt.lineHeight - 4.0))
        }
        return 0
    }
    
    fileprivate func setTitlePositionForTextAlignment() {
        let r = textRect(forBounds: bounds)
        var x = r.origin.x
        if textAlignment == NSTextAlignment.center {
            x = r.origin.x + (r.size.width * 0.5) - title.frame.size.width
        } else if textAlignment == NSTextAlignment.right {
            x = r.origin.x + r.size.width - title.frame.size.width
        }
        title.frame = CGRect(x:x, y:title.frame.origin.y, width:title.frame.size.width, height:title.frame.size.height)
    }
    
    fileprivate func showTitle(_ animated:Bool) {
        let dur = animated ? animationDuration : 0
        UIView.animate(withDuration: dur, delay:0, options: [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.curveEaseOut], animations:{
                // Animation
                self.title.alpha = 1.0
                var r = self.title.frame
                r.origin.y = self.titleYPadding
                self.title.frame = r
            }, completion:nil)
    }
    
    fileprivate func hideTitle(_ animated:Bool) {
        let dur = animated ? animationDuration : 0
        UIView.animate(withDuration: dur, delay:0, options: [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.curveEaseIn], animations:{
            // Animation
            self.title.alpha = 0.0
            var r = self.title.frame
            r.origin.y = self.title.font.lineHeight + self.hintYPadding
            self.title.frame = r
            }, completion:nil)
    }
}

//@IBDesignable class GradientColourView: UIView {
//
//}

extension CAGradientLayer {

    func backgroundGradientColor(hexColourStr:String) -> CAGradientLayer {
        let colour = UIColor.colorWithHexString(hex: hexColourStr)
        let gradientColors: [CGColor] = [colour.withAlphaComponent(0.7).cgColor, colour.cgColor]
//        let gradientLocations: [Float] = [0.0, 1.0]
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = [0.0, 1.0]
        return gradientLayer

    }
}


class DeviceLayoutConstraint: NSLayoutConstraint {

    @IBInspectable var Pixels : CGFloat = 0 {
        didSet {
           
        }
    }
    @IBInspectable var BasePixels : CGFloat = 0 {
        didSet {
           updateConstant(sizeInPixel: Pixels, withBasePixel: BasePixels)
        }
    }
    fileprivate func updateConstant(sizeInPixel: CGFloat, withBasePixel: CGFloat) {
        let phoneHeight = UIScreen.main.bounds.size.height
        let phoneHeightInpixel = phoneHeight * sizeInPixel
        let onlyPixel = phoneHeightInpixel / withBasePixel
        let pixelToPoint = onlyPixel * 0.75
        self.constant = pixelToPoint
    }
    
    //    @IBInspectable var inch3_5: CGFloat = 0.0 { didSet { updateConstant(size: .screen3_5Inch, constant: inch3_5)}}
    //    @IBInspectable var inch4: CGFloat = 0.0 { didSet { updateConstant(size: .screen4Inch, constant: inch4)}}
    //    @IBInspectable var inch4_7: CGFloat = 0.0 { didSet { updateConstant(size: .screen4_7Inch, constant: inch4_7)}}
    //    @IBInspectable var inch5_5: CGFloat = 0.0 { didSet { updateConstant(size: .screen5_5Inch, constant: inch5_5)}}
    //    @IBInspectable var inch7_9: CGFloat = 0.0 { didSet { updateConstant(size: .screen7_9Inch, constant: inch7_9)}}
    //    @IBInspectable var inch9_7: CGFloat = 0.0 { didSet { updateConstant(size: .screen9_7Inch, constant: inch9_7)}}
    //    @IBInspectable var inch12_9: CGFloat = 0.0 { didSet { updateConstant(size: .screen12_9Inch, constant: inch12_9)}}
     
//    @IBInspectable var pixelToSize : CGFloat = 0 {
//        didSet {
//
//        }
//    }
    
//    @IBInspectable var pixel_45_1920: CGFloat = 0.0 {
//        didSet {
//            updateConstant(sizeInPixel: Pixels, withBasePixel: 1920.0)
//        }
//    }
//
//    @IBInspectable var pixel_100_1920 : CGFloat = 0 {
//        didSet {
//            updateConstant(sizeInPixel: Pixels, withBasePixel: 1920.0)
//        }
//    }
//
//    @IBInspectable var pixel_45_1980: CGFloat = 0.0 {
//        didSet {
//            updateConstant(sizeInPixel: Pixels, withBasePixel: 1980.0)
//        }
//    }
//
//    @IBInspectable var pixel_100_1980 : CGFloat = 0 {
//        didSet {
//            updateConstant(sizeInPixel: Pixels, withBasePixel: 1980.0)
//        }
//    }
    
//    fileprivate func updateConstant(size: Size, constant: CGFloat) {
//        if size == deviceSize() {
//            self.constant = constant
//            layoutIfNeeded()
//        }
//    }
    
    
}

@IBDesignable
public class Gradient: UIView {
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}

    override public class var layerClass: AnyClass { CAGradientLayer.self }

    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }

    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updatePoints()
        updateLocations()
        updateColors()
    }

}
