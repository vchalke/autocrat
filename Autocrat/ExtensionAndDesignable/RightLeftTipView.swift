//
//  RightLeftTipView.swift
//  Autocrat
//
//  Created by Ios_Team on 06/01/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import UIKit

//@IBDesignable
class RightTipMessageView: UIView {
    
//    @IBInspectable
//    var borderwidth: CGFloat {
//        get {
//            return layer.borderWidth
//        }
//        set {
//            layer.borderWidth = newValue
//        }
//    }
//
//    @IBInspectable
//    var bordercolor: UIColor? {
//        get {
//            if let color = layer.borderColor {
//                return UIColor(cgColor: color)
//            }
//            return nil
//        }
//        set {
//            if let color = newValue {
//                layer.borderColor = color.cgColor
//            } else {
//                layer.borderColor = nil
//            }
//        }
//    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        
        // Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        
        // The Bezier path that we made needs to be converted to
        // a CGPath before it can be used on a layer.
        shapeLayer.path = createBezierPath().cgPath
        
        // apply other properties related to the path
        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2.5
        shapeLayer.position = CGPoint(x: 0, y: 0)
        
        // add the new layer to our custom view
        self.layer.addSublayer(shapeLayer)
    }
    
    func createBezierPath() -> UIBezierPath {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x:10, y: 50))
        path.addLine(to: CGPoint(x: 10, y: self.frame.height-30))
        path.addArc(withCenter: CGPoint(x: 20, y: self.frame.height-20),
        radius: 10,startAngle: CGFloat.pi,endAngle: CGFloat.pi*0.5,clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width-20, y: self.frame.height-10))
        path.addArc(withCenter: CGPoint(x: self.frame.width-20, y: self.frame.height-20),
                    radius: 10,startAngle: CGFloat.pi*0.5,endAngle: 0,clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width-10, y: 50))
        path.addArc(withCenter: CGPoint(x: self.frame.width-20, y: 50),
                    radius: 10,startAngle: 0,endAngle: 180,clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width*0.65, y: 40))
        path.addLine(to: CGPoint(x: self.frame.width*0.7, y: 25))
         path.addLine(to: CGPoint(x: self.frame.width*0.6, y: 40))
        path.addLine(to: CGPoint(x: 20, y: 40))
//        path.addArc(withCenter: CGPoint(x: 20, y: 50),
//                    radius: 10,startAngle: 90,endAngle: 0,clockwise: false)
        path.close()
        return path
    }
    

    /*
        func createBezierPath() -> UIBezierPath {
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x:self.frame.width*0.10, y: self.frame.height*0.2))
            path.addLine(to: CGPoint(x: self.frame.width*0.10, y: self.frame.height*0.6))
            path.addArc(withCenter: CGPoint(x: self.frame.width*0.20, y: self.frame.height*0.6),
            radius: 10,startAngle: CGFloat.pi,endAngle: CGFloat.pi*0.5,clockwise: false)
            path.addLine(to: CGPoint(x: self.frame.width*0.6, y: self.frame.height*0.7))
            path.addArc(withCenter: CGPoint(x:self.frame.width*0.6, y: self.frame.height*0.7),
                        radius: 10,startAngle: CGFloat.pi*0.5,endAngle: 0,clockwise: false)
            path.addLine(to: CGPoint(x: self.frame.width*0.8, y: self.frame.height*0.2))
            path.addArc(withCenter: CGPoint(x: self.frame.width*0.6, y: self.frame.height*0.2),
                        radius: 10,startAngle: 0,endAngle: 180,clockwise: false)
            path.addLine(to: CGPoint(x: self.frame.width*0.65, y: self.frame.height*0.2))
            path.addLine(to: CGPoint(x: self.frame.width*0.7, y: self.frame.height*0.05))
             path.addLine(to: CGPoint(x: self.frame.width*0.6, y: self.frame.height*0.2))
            path.addLine(to: CGPoint(x: self.frame.width*0.10, y: self.frame.height*0.2))
    //        path.addArc(withCenter: CGPoint(x: 20, y: 50),
    //                    radius: 10,startAngle: 90,endAngle: 0,clockwise: false)
            path.close()
            
            
            return path
        }
    
    */
    
/*
        func createBezierPath() -> UIBezierPath {
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x:5, y: 5))
            path.addLine(to: CGPoint(x: 5, y: self.frame.height-10))
            path.addArc(withCenter: CGPoint(x: 10, y: self.frame.height-10),
            radius: 5,startAngle: CGFloat.pi,endAngle: CGFloat.pi*0.5,clockwise: false)
            path.addLine(to: CGPoint(x: self.frame.width-20, y: self.frame.height-10))
            path.addArc(withCenter: CGPoint(x: self.frame.width-20, y: self.frame.height-20),
                        radius: 5,startAngle: CGFloat.pi*0.5,endAngle: 0,clockwise: false)
            path.addLine(to: CGPoint(x: self.frame.width-10, y: 50))
            path.addArc(withCenter: CGPoint(x: self.frame.width-20, y: 50),
                        radius: 5,startAngle: 0,endAngle: 180,clockwise: false)
            path.addLine(to: CGPoint(x: self.frame.width*0.65, y: 40))
            path.addLine(to: CGPoint(x: self.frame.width*0.7, y: 25))
             path.addLine(to: CGPoint(x: self.frame.width*0.6, y: 40))
            path.addLine(to: CGPoint(x: 20, y: 40))
    //        path.addArc(withCenter: CGPoint(x: 20, y: 50),
    //                    radius: 10,startAngle: 90,endAngle: 0,clockwise: false)
            path.close()
            return path
        }
 */
}



class LeftTipMessageView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        
        // Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        
        // The Bezier path that we made needs to be converted to
        // a CGPath before it can be used on a layer.
        shapeLayer.path = createBezierPath().cgPath
        
        // apply other properties related to the path
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2.5
        shapeLayer.position = CGPoint(x: 0, y: 0)
        self.layer.addSublayer(shapeLayer)
    }
    func createBezierPath() -> UIBezierPath {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x:10, y: 50))
        path.addLine(to: CGPoint(x: 10, y: self.frame.height-30))
        path.addArc(withCenter: CGPoint(x: 20, y: self.frame.height-20),
        radius: 10,startAngle: CGFloat.pi,endAngle: CGFloat.pi*0.5,clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width-20, y: self.frame.height-10))
        path.addArc(withCenter: CGPoint(x: self.frame.width-20, y: self.frame.height-20),
                    radius: 10,startAngle: CGFloat.pi*0.5,endAngle: 0,clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width-10, y: 50))
        path.addArc(withCenter: CGPoint(x: self.frame.width-20, y: 50),
                    radius: 10,startAngle: 0,endAngle: 90,clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width*0.6, y: 40))
        path.addLine(to: CGPoint(x: self.frame.width*0.45, y: 25))
         path.addLine(to: CGPoint(x: self.frame.width*0.52, y: 40))
        path.addLine(to: CGPoint(x: 20, y: 40))
//        path.addArc(withCenter: CGPoint(x: 20, y: 50),
//                    radius: 10,startAngle: 90,endAngle: 180,clockwise: false)
        path.close()
        
        
        return path
    }
    
    
}







/*
 

 func createBezierPath() -> UIBezierPath {
     
     // create a new path
     let path = UIBezierPath()

     // starting point for the path (bottom left)
     path.move(to: CGPoint(x: 2, y: 26))
     
     // *********************
     // ***** Left side *****
     // *********************
     
     // segment 1: line
     path.addLine(to: CGPoint(x: 2, y: 15))
     
     // segment 2: curve
     path.addCurve(to: CGPoint(x: 0, y: 12), // ending point
         controlPoint1: CGPoint(x: 2, y: 14),
         controlPoint2: CGPoint(x: 0, y: 14))
     
     // segment 3: line
     path.addLine(to: CGPoint(x: 0, y: 2))
     
     // *********************
     // ****** Top side *****
     // *********************
     
     // segment 4: arc
     path.addArc(withCenter: CGPoint(x: 2, y: 2), // center point of circle
         radius: 2, // this will make it meet our path line
         startAngle: CGFloat(M_PI), // π radians = 180 degrees = straight left
         endAngle: CGFloat(3*M_PI_2), // 3π/2 radians = 270 degrees = straight up
         clockwise: true) // startAngle to endAngle goes in a clockwise direction
     
     // segment 5: line
     path.addLine(to: CGPoint(x: 8, y: 0))
     
     // segment 6: arc
     path.addArc(withCenter: CGPoint(x: 8, y: 2),
                 radius: 2,
                 startAngle: CGFloat(3*M_PI_2), // straight up
         endAngle: CGFloat(0), // 0 radians = straight right
         clockwise: true)
     
     // *********************
     // ***** Right side ****
     // *********************
     
     // segment 7: line
     path.addLine(to: CGPoint(x: 10, y: 12))
     
     // segment 8: curve
     path.addCurve(to: CGPoint(x: 8, y: 15), // ending point
         controlPoint1: CGPoint(x: 10, y: 14),
         controlPoint2: CGPoint(x: 8, y: 14))
     
     // segment 9: line
     path.addLine(to: CGPoint(x: 8, y: 26))
     
     // *********************
     // **** Bottom side ****
     // *********************
     
     // segment 10: line
     path.close() // draws the final line to close the path
     
     return path
 }
 
 */
