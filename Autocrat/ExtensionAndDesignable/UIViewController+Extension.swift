//
//  UIViewController+Extension.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
import UIKit
import DropDown

extension UIViewController {
    func setupDropDown(chooseArticleButton : UITextField,dropDownSource :[String] , completion : @escaping (Int, String?) -> Void ) {
        let chooseArticleDropDown = DropDown()
        chooseArticleDropDown.anchorView = chooseArticleButton
        chooseArticleDropDown.direction = .bottom
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseArticleDropDown.backgroundColor = UIColor.init(white: 90.0, alpha: 1)
        chooseArticleDropDown.dataSource = dropDownSource
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            completion(index, item)
        }
        //        chooseArticleDropDown.dropShadow()
        chooseArticleDropDown.show()
    }
    
    
    func alertViewControllerWith(title : String, message : String,type : UIAlertController.Style,buttonArryName : [String], completion :@escaping (String)->()){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: type)
        buttonArryName.forEach { (btnName) in
            alert.addAction(UIAlertAction(title: btnName, style: .default, handler: { (UIAlertAction) in
                self.dismiss(animated:true,completion:nil)
                completion(btnName)
            }))
            
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentOnRoot(`with` viewController : UIViewController){
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
    
    func getUnique<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
extension NSLayoutConstraint {
  /**
   Change multiplier constraint
   
   - parameter multiplier: CGFloat
   - returns: NSLayoutConstraint
   */
  func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
    
    NSLayoutConstraint.deactivate([self])
    
    let newConstraint = NSLayoutConstraint(
        item: firstItem as Any,
      attribute: firstAttribute,
      relatedBy: relation,
      toItem: secondItem,
      attribute: secondAttribute,
      multiplier: multiplier,
      constant: constant)
    
    newConstraint.priority = priority
    newConstraint.shouldBeArchived = self.shouldBeArchived
    newConstraint.identifier = self.identifier
    
    NSLayoutConstraint.activate([newConstraint])
    return newConstraint
  }
    /*
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
      
      NSLayoutConstraint.deactivate([self])
      
      let newConstraint = NSLayoutConstraint(
        item: firstItem,
        attribute: firstAttribute,
        relatedBy: relation,
        toItem: secondItem,
        attribute: secondAttribute,
        multiplier: multiplier,
        constant: constant)
      
      newConstraint.priority = priority
      newConstraint.shouldBeArchived = self.shouldBeArchived
      newConstraint.identifier = self.identifier
      
      NSLayoutConstraint.activate([newConstraint])
      return newConstraint
    }
    */
    
    
}
