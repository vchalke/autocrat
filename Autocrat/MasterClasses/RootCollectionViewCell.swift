//
//  RootCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
/*
class RootCollectionViewCell<T:MasterTableViewCell,X:MasterCollectionViewCell,U,V> : UITableViewCell,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    var indexpathanimated : [IndexPath] = []
    var extraData :Any? = nil
    
    var itemTableArry = [U](){
        didSet{
            DispatchQueue.main.async {
                self.tableView?.reloadData()
            }
        }
    }
    var itemCollectionArry = [V](){
        didSet{
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }
    
    var cellheight : CGFloat = 80.0{
        didSet{
            self.tableView?.rowHeight = cellheight
        }
    }
    var cellID = ""
    var headerCellID = ""
    var width:CGFloat = 0.0
    var height:CGFloat = 0.0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    func rootCollectCellWithMultipleTableCell(tableView:UITableView,arrCellId:[String]){
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self as! UITableViewDelegate
        arrCellId.forEach { (cellID) in
            self.tableView?.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
        }
        self.tableView?.separatorStyle = .none
    }
    
    func rootCollectCellWithMultipleCollectCell(collectionView : UICollectionView, arrCellId:[String],arrHeaderId:[String]? = nil){
        self.collectionView = collectionView
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        arrCellId.forEach { (cellID) in
            self.collectionView?.register(UINib(nibName: cellID, bundle: nil), forCellWithReuseIdentifier: cellID)
        }
        arrHeaderId?.forEach { (headerID) in
            self.collectionView?.register(UINib(nibName: headerID, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerID)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemTableArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier:cellID, for: indexPath) as? MasterTableViewCell
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellheight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {}
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    private func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    private func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 0
    }
    
    //MARK: UICollectionViewDataSource,UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemCollectionArry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier:cellID, for: indexPath) as? MasterCollectionViewCell
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {}
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: width, height: height)
    }
    
}
*/
