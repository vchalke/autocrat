//
//  ProfileHeaderTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileHeaderTableViewCell: MasterTableViewCell {

    @IBOutlet weak var imgViews: UIImageView!
    
    @IBOutlet weak var lbl_EmailId: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(userdetails : User_Details){
        DispatchQueue.main.async {
            self.imgViews.layer.cornerRadius = self.imgViews.frame.height*0.5
            self.lbl_Name.text = userdetails.full_Name
            self.lbl_EmailId.text = userdetails.email_id
            self.imgViews.sd_setImage(with: userdetails.img_Url, placeholderImage: UIImage(named: "profileEmptyImg"))

        }
    }
    
}
