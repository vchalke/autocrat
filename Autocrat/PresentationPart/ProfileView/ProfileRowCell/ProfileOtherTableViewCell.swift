//
//  ProfileOtherTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import LabelSwitch
class ProfileOtherTableViewCell: UITableViewCell {

    @IBOutlet weak var switch_es: LabelSwitch!
    @IBOutlet weak var lbl_TItle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showSwitch(flag:Bool){
        self.switch_es.isHidden = !flag
        self.switch_es.isEnable = flag
    }
}
