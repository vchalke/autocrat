//
//  ProfileVC.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class ProfileVC: RootVC<ProfileTableViewCell,IntroCollectionViewCell,[String],[String]> {

    @IBOutlet weak var table_Views: UITableView!
    let optionsArr = ["New Car","Used Car","Subscriptions","Trade-in Values","Compare Deals","News & Reviews"]
    let otherOptionArr = ["Financial","Notifications","Rate Us","Contact Supports"]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: table_Views, baseController: self, arrCellId: ["ProfileHeaderTableViewCell","ProfileTableViewCell","ProfileOtherTableViewCell"])
    }
    @IBAction func btn_backPress(_ sender: Any) {
        self.setNavigatiobToRight()
//        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.pushViewController(ChooseCarHomeVC(), animated: true)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section==0) ? 0 : (section==1) ? optionsArr.count : otherOptionArr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            cell.lbl_TItle.text = optionsArr[indexPath.row]
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileOtherTableViewCell", for: indexPath) as! ProfileOtherTableViewCell
        cell.lbl_TItle.text = otherOptionArr[indexPath.row]
        cell.showSwitch(flag: (indexPath.row==1))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTableViewCell") as! ProfileHeaderTableViewCell
            if let userInfo : User_Details = AppUserdefault.shared.getStructValueForKey(key: kUserDetailInfo){
               cell.setData(userdetails: userInfo)
            }
        return cell
        }
        let viewH = UIView.init(frame: CGRect(x: 0, y: 0, width: self.table_Views.frame.width, height: 30))
        viewH.backgroundColor = UIColor.colorWithHexString(hex: "E5E5EA")
        return viewH
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.setNavigatiobToRight()
        if (indexPath.section==1 && indexPath.row==0){
            let vc = ChooseCarDetailVC()
            vc.isFromSearch = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (indexPath.section==1 && indexPath.row==1){
            let vc = ChooseCarDetailVC()
            vc.isFromSearch = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (indexPath.section==1 && indexPath.row==2){
            self.navigationController?.pushViewController(CarModelVC(), animated: true)
        }else if (indexPath.section==1 && indexPath.row==3){
            self.navigationController?.pushViewController(TradeValueVC(), animated: true)
        }else if (indexPath.section==1 && indexPath.row==4){
            self.navigationController?.pushViewController(DesignDealVC(), animated: true)
        }else if (indexPath.section==1 && indexPath.row==5){
            self.navigationController?.pushViewController(NewsReviewsVC(), animated: true)
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 120 : 30
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.section == 1) ? 60 : 50
    }

}
