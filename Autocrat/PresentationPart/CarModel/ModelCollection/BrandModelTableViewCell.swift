//
//  BrandModelTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class BrandModelTableViewCell: RootTableViewCell<PickDropTableViewCell,TopStoryCollectionViewCell,[String],[String]> {

    @IBOutlet weak var topCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setTableViewData(){
        self.inTableWithMultipleCollectionCell(collectionView : self.topCollectionView,cellid:"IntroCollectionViewCell")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
        cell.imgViews.image = UIImage.init(named: "HomeCarBottom\(indexPath.row)")
        return cell
        
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.topCollectionView.frame.width*0.62, height: self.topCollectionView.frame.height)
    }
    
}
