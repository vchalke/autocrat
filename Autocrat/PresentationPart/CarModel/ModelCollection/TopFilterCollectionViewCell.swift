//
//  TopFilterCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class TopFilterCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var imgViews: UIImageView!
    @IBOutlet weak var btn_Prize: UIButton!
    @IBOutlet weak var leftViews: UIView!
    @IBOutlet weak var rightViews: UIView!
    @IBOutlet weak var cnstLeftViewWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func hideRight(flag:Bool){
        self.leftViews.isHidden = !flag
        self.rightViews.isHidden = flag
        self.cnstLeftViewWidth = self.cnstLeftViewWidth.setMultiplier(multiplier: flag ? 1.0 : 0.01)
    }

}
