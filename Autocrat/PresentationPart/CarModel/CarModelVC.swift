//
//  CarModelVC.swift
//  Autocrat
//
//  Created by Ios_Team on 18/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class CarModelVC: RootVC<PickDropTableViewCell,TopFilterCollectionViewCell,[String],[String]>  {

    let titleArr = ["All Price","All Brands","Fuel Brand","All Price","All Brands","Fuel Brand"]
    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var modelTableVews: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.modelTableVews, baseController: self, arrCellId: ["BrandModelTableViewCell"])
        self.initCollectionViewMultipleCell(collectionView: self.topCollectionView, baseController: self, arrCellId: ["TopFilterCollectionViewCell"])
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    @IBAction func backPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandModelTableViewCell", for: indexPath) as! BrandModelTableViewCell
        cell.setTableViewData()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopFilterCollectionViewCell", for: indexPath) as! TopFilterCollectionViewCell
        cell.btn_Prize.setTitle(titleArr[indexPath.row], for: .normal)
        cell.imgViews.image = UIImage.init(named: "downAngle")
        cell.hideRight(flag: (indexPath.row==0))
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (indexPath.row==0) ? self.topCollectionView.frame.width*0.18 : self.topCollectionView.frame.width*0.38
        return CGSize(width: width , height: self.topCollectionView.frame.height)
    }

}
