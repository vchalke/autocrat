//
//  DummyTextVC.swift
//  Autocrat
//
//  Created by Ios_Team on 18/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class DummyTextVC: MasterVC {

    @IBOutlet weak var btn_Next: UIButton!
    @IBOutlet weak var lbl_First: UILabel!
    @IBOutlet weak var lbl_Second: UILabel!
    @IBOutlet weak var lbl_Third: UILabel!
    var index = 0
    var isQualification = false
    let titleArr = ["Pick Up\nOr\nDelievery","Legal Name\nAddress\nDriver License","Profession\nEmployer\nType(FT/PT)\nIncome\nStatus","Loan Goals\nVehicle\nTypes"]
    let smallTitleArr = ["","","(Citizen, Resident, Veteran,\netc.)",""]
    let fieldTitleArr = ["","[Fields]","[Fields]",""]
    let qualifyTitleArr = ["Required\nConfirm\nEmployment\nInformation","Required\nConfirm\nPrimary\nAddress","Required\nConfirmation\nQuiz/Survey","Required\nAdd Bank(s)\nAdd Card(s)"]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setInfoInLabel(index: index)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.btn_Next.layer.cornerRadius = self.btn_Next.frame.height*0.5
    }

    @IBAction func btn_NextPressed(_ sender: Any) {
        index+=1
        (index<4) ? self.setInfoInLabel(index: index) : self.jumpToNextPage()
    }
    
    func setInfoInLabel(index:Int){
        self.lbl_First.text = isQualification ? qualifyTitleArr[index] : titleArr[index]
        self.lbl_Second.text = isQualification ? "" : smallTitleArr[index]
        self.lbl_Third.text = isQualification ? "" : fieldTitleArr[index]
    }
    func jumpToNextPage(){
        self.jumpToController(vControlller: isQualification ? FundingOulookVC() : ProfileVC())
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
