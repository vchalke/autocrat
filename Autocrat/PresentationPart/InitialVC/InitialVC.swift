//
//  InitialVC.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class InitialVC: MasterVC {

    @IBOutlet weak var btn_SignUp: UIButton!
    @IBOutlet weak var btn_SignIn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideBothButton(flag: true)
        self.setCorenerRadius()
        let deadlineTime = DispatchTime.now() + .seconds(2)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            print("test")
            if let userInfo : User_Details = AppUserdefault.shared.getStructValueForKey(key: kUserDetailInfo){
                printLog(userInfo)
                self.jumpToController(vControlller:ChooseCarHomeVC())
            }else{
                self.hideBothButton(flag: false)
//                self.jumpToController(vControlller:SignInOptionsVC())
            }
            
        }
    }

    func hideBothButton(flag:Bool){
        self.btn_SignUp.isHidden = flag
        self.btn_SignIn.isHidden = flag
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func setCorenerRadius(){
        DispatchQueue.main.async {
            self.btn_SignIn.layer.cornerRadius = self.btn_SignIn.frame.height*0.5
            self.btn_SignUp.layer.cornerRadius = self.btn_SignUp.frame.height*0.5
            
        }
    }
    
    @IBAction func btnSignUpPress(_ sender: Any) {
       let viewC = SignUpVC()
        viewC.currentScreenID = kSignUP // kSignUP  kSignIn
         self.jumpToController(vControlller:viewC)
    }
    @IBAction func btnSignInPress(_ sender: Any) {
       let viewC = SignUpVC()
       viewC.currentScreenID = kSignIn // kSignUP  kSignIn
        self.jumpToController(vControlller:viewC)
    }
}
