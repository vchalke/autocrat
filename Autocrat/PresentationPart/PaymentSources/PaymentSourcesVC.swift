//
//  PaymentSourcesVC.swift
//  Autocrat
//
//  Created by Ios_Team on 20/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class PaymentSourcesVC: RootVC<SelectBudgetTableViewCell,TopFilterCollectionViewCell,[String],[String]> {

    @IBOutlet weak var btn_PayNow: UIButton!
    let paymentOptionArr = ["Paypal","Google Wallet","Amazon Pay","Credit / Debit Card","Bank Tranfer"]
    @IBOutlet weak var tableViewa: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTableViewMultipleCell(tableView: self.tableViewa, baseController: self, arrCellId: ["SelectBudgetTableViewCell","CreateListHeaderTableViewCell","VehicleInfoTableViewCell","StartFromTableViewCell"])
        self.tableViewa?.separatorStyle = .singleLine
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.btn_PayNow.layer.cornerRadius = self.btn_PayNow.frame.height*0.5
    }
    
    @IBAction func btnPayNowPressed(_ sender: Any) {
        let confVc = ConfirmPayVC()
        confVc.isFromPaySource = true
        self.navigationController?.pushViewController(confVc, animated: true)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? 5 : 3
        
    }
    @IBAction func backPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 1 && indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleInfoTableViewCell", for: indexPath) as! VehicleInfoTableViewCell
            cell.lbl_One.text = "Card Number"
            cell.lbl_Two.text = "1234 1234 1234 1234"
            return cell
        }else if (indexPath.section == 1 && indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
            cell.lbl_firstTitle.text = "Expiry/Validity"
            cell.lbl_firstDescription.text = "12/21"
            cell.lbl_secondTitle.text = "cvv"
            cell.lbl_secondDescription.text = "123"
            cell.btn_Middle.isHidden = true
            return cell
        }else if (indexPath.section == 1 && indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleInfoTableViewCell", for: indexPath) as! VehicleInfoTableViewCell
            cell.lbl_One.text = "Name On Card"
            cell.lbl_Two.text = "Craig Bellamy"
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectBudgetTableViewCell", for: indexPath) as! SelectBudgetTableViewCell
        cell.lbl_Titles.text = paymentOptionArr[indexPath.row]
        cell.btn_Select.setImage(UIImage.init(named: "NoSelectCircular"), for: .normal)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellView = tableView.dequeueReusableCell(withIdentifier: "CreateListHeaderTableViewCell") as! CreateListHeaderTableViewCell
        cellView.lbl_Title.text = (section == 0) ? "" : "Credit Card"
        return cellView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 25 : 70
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.section == 0) ? 60 : 85
    }

}
