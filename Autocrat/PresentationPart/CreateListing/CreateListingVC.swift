//
//  CreateListingVC.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class CreateListingVC: RootVC<CreateListHeaderTableViewCell,IntroCollectionViewCell,[String],[String]> {

    @IBOutlet weak var btn_SelectCar: UIButton!
    @IBOutlet weak var tble_Views: UITableView!
    let sectionTitle = ["Enter Basic Vehicle Details", "Vehicle Info"]
    let rowInfoArr = [["License Plate", "QWE 234"],["Registration Year", "2015"],["Transmission", "Automatic"],["Mileage (KM)", "1450"],["Qwner", "First"]]
    var scrollIndex:Int?
    var txtClicked = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTableViewMultipleCell(tableView: self.tble_Views, baseController: self, arrCellId: ["CreateListHeaderTableViewCell","VehicleInfoTableViewCell","SellerTypeTableViewCell"])

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.btn_SelectCar.layer.cornerRadius = self.btn_SelectCar.frame.height*0.5
    }

    // MARK: Keyboard Notifications
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            /*
            if(txtClicked){
                if let scollindex = self.scrollIndex{
                    if scollindex > 0{
                        tble_Views.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
                        let indexPath = IndexPath(row: scollindex, section: 0)
                        DispatchQueue.main.async {
                            self.tble_Views.scrollToRow(at: indexPath, at: .middle, animated: false)
                        }
                    }
                }
            }
            */
            self.tble_Views.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
       @objc func keyboardWillHide(notification: NSNotification) {
           UIView.animate(withDuration: 0.2, animations: {
               // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
               self.tble_Views.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
               
           })
       }
    
    @IBAction func btn_BackPress(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_SelectCarPress(_ sender: Any) {
        self.setNavigatiobToRight()
        let vc = ChooseCarDetailVC()
        vc.isFromSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitle.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section==0) ? 1 : rowInfoArr.count    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SellerTypeTableViewCell", for: indexPath) as! SellerTypeTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleInfoTableViewCell", for: indexPath) as! VehicleInfoTableViewCell
        cell.lbl_One.text = rowInfoArr[indexPath.row][0]
//        cell.lbl_Two.text = rowInfoArr[indexPath.row][0]
        cell.txtFields.text = rowInfoArr[indexPath.row][1]
        cell.vehiDelegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellView = tableView.dequeueReusableCell(withIdentifier: "CreateListHeaderTableViewCell") as! CreateListHeaderTableViewCell
        cellView.lbl_Title.text = sectionTitle[section]
        return cellView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.section == 0) ? 120 : 90
    }
    

}

extension CreateListingVC : VehicleInfoTableViewCellDelegate{
    func indexClicked(index: Int, txtField: UITextField) {
        scrollIndex = index
        txtClicked = true
    }
    
    
}
