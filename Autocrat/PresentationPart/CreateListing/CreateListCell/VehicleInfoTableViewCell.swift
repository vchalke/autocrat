//
//  VehicleInfoTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol VehicleInfoTableViewCellDelegate {
    func indexClicked(index:Int, txtField:UITextField)
}
class VehicleInfoTableViewCell: MasterTableViewCell {

    @IBOutlet weak var basesView: UIView!
    @IBOutlet weak var lbl_One: UILabel!
    @IBOutlet weak var lbl_Two: UILabel!
    @IBOutlet weak var txtFields
    : UITextField!
    var vehiDelegate:VehicleInfoTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.txtFields.delegate = self
        self.backgroundColor = UIColor.colorWithHexString(hex: "F6F6F6")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension VehicleInfoTableViewCell : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.basesView.endEditing(true)
       // self.nextTextField(textField: textField)
        return false
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.txtFields.resignFirstResponder()
        self.vehiDelegate?.indexClicked(index: textField.tag, txtField: textField)
        printLog("textfielddidbegin\(textField)")
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        printLog("textfildclear\(textField)")
        return true
    }
}
