//
//  ChatVC.swift
//  Autocrat
//
//  Created by Ios_Team on 17/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class ChatVC: RootVC<ChatTableViewCell,IntroCollectionViewCell,[String],[String]> {

//    let msgArr = ["Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu","consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."]
    
    @IBOutlet weak var bottomViews: DelieveryFooterView!
    @IBOutlet weak var cnst_heightOfMsgView: NSLayoutConstraint!
    @IBOutlet weak var txtMesgView: UITextView!
    @IBOutlet weak var tableChatViews: UITableView!
    var messages = [Message]()
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.initTableViewMultipleCell(tableView: self.tableViews, baseController: self, arrCellId: ["ChatTableViewCell"])
        self.txtMesgView.delegate = self
        self.bottomViews.delegate = self
        self.initTableViewMultipleCell(tableView: self.tableChatViews, baseController: self, arrCellId: ["RightMsgTableViewCell","LeftMsgTableViewCell"])
        self.fetchData()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.tableChatViews.addGestureRecognizer(tap)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismissKeyBoard()
    }
    func dismissKeyBoard(){
        self.txtMesgView.resignFirstResponder()
        self.cnst_heightOfMsgView.constant = 0.0
    }
    func fetchData() {
        messages = MessageStore.getAll()
        tableChatViews.reloadData()
        tableChatViews.transform = CGAffineTransform(scaleX: 1, y: -1)
    }

    
    @IBAction func backPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func locatePressed(_ sender: Any) {
        self.setNavigatiobToRight()
        self.navigationController?.pushViewController(LocateVehicleVC(), animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    /*override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        cell.lbl_UserMsg.text = msgArr[indexPath.row]
        cell.showManagerView(flag: (indexPath.row%2 == 0))
        return cell
    }*/
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        if message.side == .left {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMsgTableViewCell") as! LeftMsgTableViewCell
            cell.lbl_LeftMsg.text = message.text
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RightMsgTableViewCell") as! RightMsgTableViewCell
            cell.lbl_RightMsg.text = message.text
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            return cell
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        let cell = self.tableViews.cellForRow(at: indexPath)as! ChatTableViewCell
    //        let height : CGFloat = (CGFloat(17 * cell.lbl_UserMsg.numberOfLines))
            return UITableView.automaticDimension //height+40.0 //200
        }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            printLog(keyboardSize)
            printLog(self.view.safeAreaInsets.bottom)
            self.cnst_heightOfMsgView.constant = keyboardSize.height-80-self.view.safeAreaInsets.bottom
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.cnst_heightOfMsgView.constant = 0.0
    }
    @IBAction func btn_SendPressed(_ sender: Any) {
        if let str = self.txtMesgView.text{
            if (str.count > 0){
                let msg = Message(text: str, side: .right)
                messages.insert(msg, at: 0)
                
                let msgOne = Message(text: "Wait for a minute", side: .left)
                messages.insert(msgOne, at: 0)
            }
        }
        self.dismissKeyBoard()
        self.txtMesgView.text = ""
        self.tableChatViews.reloadData()
    }
}
extension ChatVC: DelieveryFooterViewDelegate{
    func bottomOPtionClicked(index: Int) {
        self.selectedBottomOption = index
        self.jumpToBottomOption(index: index)
    }
}
extension ChatVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        printLog("print1")
        self.txtMesgView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        printLog("print2")
        
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        printLog("textViewShouldEndEditing")
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         if(text == "\n") {
//             addRemBorder(to: textView, flag: false)
             textView.resignFirstResponder()
             return false
         }
         return true
     }
}
struct MessageStore {
    static func getAll() -> [Message] {
        return [
            Message(text: "Lorem ipsum dolor sit amet", side: .left),
            Message(text: "Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", side: .left),
            Message(text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae", side: .right),
            Message(text: "Excepteur sint occaecat cupidatat non proident", side: .left),
            Message(text: "Sed ut perspiciatis", side: .right),
            Message(text: "qui dolorem ipsum quia dolor sit amet, consectetur", side: .left),
            Message(text: "Vel illum qui dolorem eum fugiat", side: .right),
            Message(text: "At vero eos et accusamus et iusto odio dignissimos", side: .left),
            Message(text: "Et harum quidem rerum facilis", side: .right),
            Message(text: "Lorem ipsum dolor sit amet", side: .left),
            Message(text: "Vel illum qui dolorem eum fugiat", side: .left),
            Message(text: "Lorem ipsum", side: .left),
            Message(text: "Lorem ipsum", side: .right),
            Message(text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", side: .right),
            Message(text: "Et harum quidem rerum facilis", side: .right),
        ]
    }
}


extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}




enum MessageSide {
    case left
    case right
}

struct Message {
    var text = ""
    var side: MessageSide = .right
}

struct Chat {
    var title = ""
    var imageName = ""
    var latestMessage = ""
    var sender: String? = nil
}

