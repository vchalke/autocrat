//
//  ChatTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class ChatTableViewCell: MasterTableViewCell {

    @IBOutlet weak var view_ManagerMsg: UIView!
    @IBOutlet weak var view_UserMsg: UIView!
    
    @IBOutlet weak var lbl_ManageMsg: UILabel!
    @IBOutlet weak var lbl_UserMsg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showManagerView(flag:Bool){
        self.view_ManagerMsg.isHidden = !flag
        self.view_UserMsg.isHidden = flag
    }
}
