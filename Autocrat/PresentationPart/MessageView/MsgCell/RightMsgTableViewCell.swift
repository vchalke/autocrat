//
//  RightMsgTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 06/01/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import UIKit
protocol RightMsgTableViewCellDelegate {
    
}
class RightMsgTableViewCell: MasterTableViewCell {

    @IBOutlet weak var lbl_RightMsg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
