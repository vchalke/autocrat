//
//  LoadingVC.swift
//  Autocrat
//
//  Created by Ios_Team on 23/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {
    
    @IBOutlet weak var lblTItle: UILabel!
    @IBOutlet weak var lblSubTItle: UILabel!
    @IBOutlet weak var bseImgView: UIView!
    @IBOutlet weak var imgVIews: UIImageView!
    @IBOutlet weak var bseMsgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideLoaderImage(flag: false)
        self.addAnimationOnImage()
        NotificationCenter.default.addObserver(self, selector: #selector(self.successResponseNotification(notification:)), name: Notification.Name(NotificationCentersConstant.SUCCESS.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.failureInServerNotification(notification:)), name: Notification.Name(NotificationCentersConstant.FAILURE_IN_SERVER.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.failureInAPINotification(notification:)), name: Notification.Name(NotificationCentersConstant.FAILURE_IN_API.rawValue), object: nil)
    }
    
    func addAnimationOnImage(){
        let rotation = CABasicAnimation(keyPath: "transform.rotation.y")
        rotation.fromValue = 0
        rotation.toValue = 2 * CGFloat.pi
        rotation.duration = 1.0
        rotation.repeatCount = Float.infinity
        bseImgView.layer.add(rotation, forKey: "Spin")
    }
    @objc func successResponseNotification(notification: Notification) {
         self.navigationController?.dismiss(animated: true, completion: nil)
        
    }
    @objc func failureInServerNotification(notification: Notification) {
        self.hideLoaderImage(flag: true)
        self.lblTItle.text = "TRY AGAIN !"
        self.lblSubTItle.text = self.getStringOfAllValues(dictionary: notification.userInfo as! [String : Any])
        
    }
    @objc func failureInAPINotification(notification: Notification) {
        self.hideLoaderImage(flag: true)
        self.lblTItle.text = "Something Went Wrong.."
        self.lblSubTItle.text = self.getStringOfAllValues(dictionary: notification.userInfo as! [String : Any])
        
    }
    
    func hideLoaderImage(flag:Bool){
        self.bseImgView.isHidden = flag
        self.bseMsgView.isHidden = !flag
    }
    
    @IBAction func btmDismissPress(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func getStringOfAllValues(dictionary:[String:Any])->String{
        var mutString = ""
        for (key, value) in dictionary {
            mutString.append("\(value)\n")
        }
        return mutString
    }
    
    
    
}
