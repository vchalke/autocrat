//
//  LoaderView.swift
//  Autocrat
//
//  Created by Ios_Team on 23/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class LoaderView {
    
    static var currentOverlay : UIView?
    static var currentOverlayTarget : UIView?
    static var currentLoadingText: String?
    
//    lazy var baseView : UIView = {
//        let frame = CGRect(x: kFrameWidth * 0.05, y: kFrameHeight*0.5, width: kFrameWidth * 0.9, height: kFrameHeight*0.35)
//        let view = UIView.init(frame:frame)
//        view.backgroundColor = .white
//        return view
//    }()
//    lazy var lblMessage : UILabel = {
//        let lbl = UILabel.init(frame:baseView.frame)
//        lbl.font = UIFont.systemFont(ofSize: 14, weight: .medium)
//        lbl.textColor = .black
//        lbl.textAlignment = .center
//        lbl.numberOfLines = 0
//        lbl.text = "Loading..."
//        return lbl
//    }()
    
    static func showLoader() {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            print("No main window.")
            return
        }
        showLoader(currentMainWindow)
    }
    
    static func showLoader(_ loadingText: String) {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            print("No main window.")
            return
        }
        showLoader(currentMainWindow, loadingText: loadingText)
    }
    
    static func showLoader(_ overlayTarget : UIView) {
        showLoader(overlayTarget, loadingText: nil)
    }
    
    static func showLoader(_ overlayTarget : UIView, loadingText: String?) {
        // Clear it first in case it was already shown
        hideLoader()
        
        // register device orientation notification
        NotificationCenter.default.addObserver(
            self, selector:
            #selector(LoaderView.rotated),
            name: UIDevice.orientationDidChangeNotification,
            object: nil)
        
        // Create the overlay
        let overlay = UIView(frame: overlayTarget.frame)
        overlay.center = overlayTarget.center
        overlay.alpha = 0
        overlay.backgroundColor = UIColor.black
        overlayTarget.addSubview(overlay)
        overlayTarget.bringSubviewToFront(overlay)
        overlay.alpha = overlay.alpha > 0 ? 0 : 0.5
        
        //         Create and animate the activity indicator
        //        let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        //        indicator.center = overlay.center
        //        indicator.startAnimating()
        //        overlay.addSubview(indicator)
        
        //         Create and animate the activity indicator
//        let indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .ballClipRotateMultiple, color: .white, padding: 0)
//        indicator.center = overlay.center
//        indicator.startAnimating()
//        overlay.addSubview(indicator)
        
//        flatCarIconOne
        let imgFrame = CGRect(x: 0, y: 0, width: 60, height: 60)
        let imgBaseView = UIView.init(frame: imgFrame)
        let imgView = UIImageView.init(frame: imgFrame)
        imgBaseView.center = overlay.center
        imgView.image = UIImage.init(named: "flatCarIconOne")
        imgBaseView.addSubview(imgView)
        overlay.addSubview(imgBaseView)
        
//        UIView.animate(withDuration: 0.25, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
//            imgBaseView.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 0.25)
//        }, completion: nil)
        
         let rotation = CABasicAnimation(keyPath: "transform.rotation.y")
         rotation.fromValue = 0
         rotation.toValue = 2 * CGFloat.pi
         rotation.duration = 1.0
         rotation.repeatCount = Float.infinity
         imgBaseView.layer.add(rotation, forKey: "Spin")
        
//        var perspectiveTransform = CATransform3DIdentity
//        perspectiveTransform.m34 = -0.002
//        let rotateAngle = 0.5 * CGFloat(Double.pi)
//        perspectiveTransform = CATransform3DTranslate(perspectiveTransform, 0, 0, 300)
//        imgBaseView.layer.transform = CATransform3DRotate(perspectiveTransform, rotateAngle, 0, 1, 0)
        
        
        //         Create label
//        if let textString = loadingText {
//            let label = UILabel()
//            label.text = textString
//            label.textColor = UIColor.white
//            label.sizeToFit()
//            label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 50)
//            overlay.addSubview(label)
//        }
        
        
        // Animate the overlay to show
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        
        UIView.commitAnimations()
        
        currentOverlay = overlay
        currentOverlayTarget = overlayTarget
        currentLoadingText = loadingText
    }
    
    static func hideLoader() {
        if currentOverlay != nil {
            
            // unregister device orientation notification
            NotificationCenter.default.removeObserver(self,                                                      name: UIDevice.orientationDidChangeNotification,                                                      object: nil)
            
            currentOverlay?.removeFromSuperview()
            currentOverlay =  nil
            currentLoadingText = nil
            currentOverlayTarget = nil
        }
    }
    
    @objc private static func rotated() {
        // handle device orientation change by reactivating the loading indicator
        if currentOverlay != nil {
            showLoader(currentOverlayTarget!, loadingText: currentLoadingText)
        }
    }
    
    
    
    
}
