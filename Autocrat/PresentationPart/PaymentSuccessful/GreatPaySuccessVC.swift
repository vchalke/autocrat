//
//  GreatPaySuccessVC.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class GreatPaySuccessVC: RootVC<ConfirmInfoTableViewCell,IntroCollectionViewCell,[String],[String]>  {

    @IBOutlet weak var paySuccesstblViews: UITableView!
    @IBOutlet weak var curveViews: UIView!
    
    @IBOutlet weak var btn_Done: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.paySuccesstblViews, baseController: self, arrCellId: ["ConfirmInfoTableViewCell","PickDropTableViewCell","StartFromTableViewCell","DailNotifTableViewCell"])
    }


    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.curveViews.roundCorners(corners: [.bottomLeft , .bottomRight], radius: self.curveViews.frame.height)
        self.paySuccesstblViews.layer.cornerRadius = 15.0
        self.btn_Done.layer.cornerRadius = self.btn_Done.frame.height*0.5
    }
    
    @IBAction func btnDonePressed(_ sender: Any) {
        self.navigationController?.pushViewController(SelectDeliveryVC(), animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmInfoTableViewCell", for: indexPath) as! ConfirmInfoTableViewCell
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PickDropTableViewCell", for: indexPath) as! PickDropTableViewCell
            return cell
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailNotifTableViewCell", for: indexPath) as! DailNotifTableViewCell
        cell.switchView.isEnable = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 300 : 90
    }

}
