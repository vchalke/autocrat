//
//  IntroCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class IntroCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var imgViews: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setCornerTo(radius:CGFloat){
        self.imgViews.layer.cornerRadius = radius
    }

}
