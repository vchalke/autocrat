//
//  IntroductionVC.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class IntroductionVC: RootVC<ProfileTableViewCell,IntroCollectionViewCell,[String],[String]> {

    @IBOutlet weak var btn_Skip: UIButton!
    @IBOutlet weak var collection_Views: UICollectionView!
    @IBOutlet weak var pg_Contrls: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initCollectionViewMultipleCell(collectionView: self.collection_Views, baseController: self, arrCellId: ["IntroCollectionViewCell"])
    }


    @IBAction func btn_SkipPress(_ sender: Any) {
        self.jumpToController(vControlller:WelcomeScreenBVC())
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
        cell.imgViews.image = UIImage.init(named: "IntroVCBack")
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collection_Views.frame.width, height: self.collection_Views.frame.height)
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        print("page = \(page)")
        self.pg_Contrls.currentPage = page
    }
}
