//
//  WelcomeQuesVC.swift
//  Autocrat
//
//  Created by Ios_Team on 21/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class WelcomeQuesVC: RootVC<VehicleInfoTableViewCell,IntroCollectionViewCell,[String],[String]> {


    @IBOutlet weak var btn_Next: UIButton!
    let titleArray = ["QUESTIONS #1","QUESTIONS #2","QUESTIONS #3","QUESTIONS #4","QUESTIONS #5"]
    let subTitleArray = ["[DROP DOWN]","[DROP DOWN]","[DROP DOWN]","[DROP DOWN]","[YES OR NO RADIO BUTTONS]"]
    @IBOutlet weak var tblQuesView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

       
        self.setRadiusOfCorners()
        self.initTableViewMultipleCell(tableView: self.tblQuesView, baseController: self, arrCellId: ["StartFromTableViewCell","VehicleInfoTableViewCell"])
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func skipPressed(_ sender: Any) {
        self.jumpToNextScreen()
       }
    @IBAction func nextPressed(_ sender: Any) {
        self.jumpToNextScreen()
       }
    
    func jumpToNextScreen(){
        let bValue = AppUserdefault.shared.getBoolValueForKey(key: kIsFromHomeVC) ?? false
        self.navigationController?.pushViewController(bValue ? WelcomeBankDetailVC() : WelcomeProfessionScrVC(), animated: true)
    }
    
    func setRadiusOfCorners(){
        self.btn_Next.layer.cornerRadius = self.btn_Next.frame.height*0.5
       
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.tblQuesView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //tableViewBottom.constant = 0.0
        self.tblQuesView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
        
    }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleInfoTableViewCell", for: indexPath) as! VehicleInfoTableViewCell
            cell.lbl_One.text = titleArray[indexPath.row]
            cell.txtFields.text = subTitleArray[indexPath.row]
            cell.txtFields.autocapitalizationType = .words;
            return cell
        }


}
