//
//  WelcomeBankDetailVC.swift
//  Autocrat
//
//  Created by Ios_Team on 21/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class WelcomeBankDetailVC: RootVC<VehicleInfoTableViewCell,IntroCollectionViewCell,[String],[String]> {


    @IBOutlet weak var btn_Add: UIButton!
    
    let titleArray = ["TYPE","BANK NAME","ACCOUNT","ROUTING","NICKNAME"]
    let subTitleArray = ["[DROP DOWN LIST]","[DROP DOWN LIST]","01234547154545","147852369","[FORT KNOX]"]
    @IBOutlet weak var tblBankDetail: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

       
        self.setRadiusOfCorners()
        self.initTableViewMultipleCell(tableView: self.tblBankDetail, baseController: self, arrCellId: ["StartFromTableViewCell","VehicleInfoTableViewCell"])
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func skipPressed(_ sender: Any) {
        self.navigationController?.pushViewController(FundingOulookVC(), animated: true)
       }
    @IBAction func addPressed(_ sender: Any) {
        self.navigationController?.pushViewController(FundingOulookVC(), animated: true)
       }
    func setRadiusOfCorners(){
        self.btn_Add.layer.cornerRadius = self.btn_Add.frame.height*0.5
       
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.tblBankDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //tableViewBottom.constant = 0.0
        self.tblBankDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
        
    }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if (indexPath.row == 3){
                let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
                cell.lbl_firstTitle.text = "HOW MANY YEARS"
                cell.lbl_firstDescription.text = "[DROP DOWN]"
                cell.lbl_secondTitle.text = "STATUS"
                cell.lbl_secondDescription.text = "[FT/PTE]"
                cell.btn_Middle.isHidden = true
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleInfoTableViewCell", for: indexPath) as! VehicleInfoTableViewCell
            cell.lbl_One.text = titleArray[indexPath.row]
            cell.txtFields.text = subTitleArray[indexPath.row]
            cell.txtFields.autocapitalizationType = .words;
            return cell
        }


}
