//
//  WelcomeScreenBVC.swift
//  Autocrat
//
//  Created by Ios_Team on 21/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class WelcomeScreenBVC: MasterVC {

    @IBOutlet weak var btn_Skip: UIButton!
    @IBOutlet weak var btn_PickUp: UIButton!
    @IBOutlet weak var btn_Delivery: UIButton!
    @IBOutlet weak var btn_NotSure: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setCornerRadius()
    }


    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    func setCornerRadius(){
        self.btn_PickUp.layer.cornerRadius = self.btn_PickUp.frame.height*0.5
        self.btn_Delivery.layer.cornerRadius = self.btn_Delivery.frame.height*0.5
        self.btn_NotSure.layer.cornerRadius = self.btn_NotSure.frame.height*0.5
    }
    @IBAction func btn_BackPressed(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_NotSure(_ sender: Any) {
        self.jumpToWelcomeScreenC()
    }
    func jumpToWelcomeScreenC(){
        self.jumpToController(vControlller: WelcomeNameScrVC())
    }
    @IBAction func btn_Skip(_ sender: Any) {
        self.jumpToWelcomeScreenC()
    }
    
}
