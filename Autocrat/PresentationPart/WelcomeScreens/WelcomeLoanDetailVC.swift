//
//  WelcomeLoanDetailVC.swift
//  Autocrat
//
//  Created by Ios_Team on 21/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class WelcomeLoanDetailVC: RootVC<VehicleInfoTableViewCell,IntroCollectionViewCell,[String],[String]> {


    @IBOutlet weak var btn_Next: UIButton!
    let titleArray = ["LOAN AMOUNT SWEEKING","TYPE OF VEHICLE","CASH DOWN (OPTIONAL)","FIRST VEHICLE","WHEN WILL YOU PURCHASE"]
    let subTitleArray = ["[DROP DOWN]","[DROP DOWN RANGE]","[MANUAL FIELD]","[DROP DOWN]","[DROP DOWN]"]
    var headerArr : [String]?
    @IBOutlet weak var tablLoanDetail: UITableView!
    override func viewDidLoad() {
//        super.viewDidLoad()
       let filePath = baseFilePath+FileName.LOAN_INFO.rawValue+"_copy.csv"
        if let arra = CSVFilesReaders.getHeaderArr(fileName: filePath){
           headerArr = arra
       }
        self.setRadiusOfCorners()
        self.initTableViewMultipleCell(tableView: self.tablLoanDetail, baseController: self, arrCellId: ["StartFromTableViewCell","VehicleInfoTableViewCell"])
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }

    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func skipPressed(_ sender: Any) {
        self.navigationController?.pushViewController(ProfileVC(), animated: true)
       }
    @IBAction func nextPressed(_ sender: Any) {
        self.navigationController?.pushViewController(ProfileVC(), animated: true)
       }
    func setRadiusOfCorners(){
        self.btn_Next.layer.cornerRadius = self.btn_Next.frame.height*0.5
       
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.tablLoanDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //tableViewBottom.constant = 0.0
        self.tablLoanDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       let rowCount = headerArr?.count ?? 0
        return rowCount
        
    }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if (indexPath.row == 3){
                let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
                cell.lbl_firstTitle.text = "FIRST VEHICLE"
//                cell.lbl_firstDescription.text = "[DROP DOWN]"
                cell.lbl_secondTitle.text = "STATUS"
//                cell.lbl_secondDescription.text = "[FT/PTE]"
                cell.btn_Middle.isHidden = true
                cell.txt_firstDesc.text = "[DROP DOWN]"
                cell.txt_secondDesc.text = "[FT/PTE]"
                cell.txt_firstDesc.tag = indexPath.row
                cell.txt_secondDesc.tag = indexPath.row+1
                cell.delegate = self
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleInfoTableViewCell", for: indexPath) as! VehicleInfoTableViewCell
            cell.lbl_One.text = titleArray[indexPath.row]
            cell.txtFields.text = subTitleArray[indexPath.row]
            cell.txtFields.autocapitalizationType = .words;
            cell.txtFields.tag = indexPath.row
            cell.vehiDelegate = self
            return cell
        }


}


extension WelcomeLoanDetailVC:VehicleInfoTableViewCellDelegate,StartFromTableViewCellDelegate{
    func txtFieldClicked(index: Int, txtField: UITextField) {
        self.setTextField(index: index, txtField: txtField)
    }
    
    func indexClicked(index: Int, txtField:UITextField) {
        self.setTextField(index: index, txtField: txtField)
    }
    
    func setTextField(index: Int, txtField:UITextField){
        let filePath = baseFilePath+FileName.LOAN_INFO.rawValue+"_copy.csv"
        if let strArr = CSVFilesReaders.getArrayFromFile(fileName: filePath, key: headerArr?[index] ?? ""){
            self.setupDropDown(chooseArticleButton: txtField, dropDownSource: self.getUnique(source: strArr)) { (index, selectStr) in
                txtField.text = (selectStr?.count ?? 0 > 0) ? selectStr : "[DROP DOWN]"
            }
        }
    }
    
}
