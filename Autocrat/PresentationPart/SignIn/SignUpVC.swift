//
//  SignUpVC.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FBSDKLoginKit
class SignUpVC: MasterVC {

    @IBOutlet weak var view_Name: UIView!
    @IBOutlet weak var view_Email: UIView!
    @IBOutlet weak var view_Password: UIView!
    @IBOutlet weak var btn_SignUp: UIButton!
    @IBOutlet weak var btn_ForGotPass: UIButton!
    @IBOutlet weak var lbl_ScreenTitle: UILabel!
    @IBOutlet weak var cnst_ViewHeight: NSLayoutConstraint!
    let btn_FBLogin = FBLoginButton()
    
    @IBOutlet weak var view_SocialLogin: UIView!
    @IBOutlet weak var img_google: UIImageView!
    @IBOutlet weak var img_faceBook: UIImageView!
    @IBOutlet weak var view_google: UIView!
    @IBOutlet weak var view_faceBook: UIView!
    @IBOutlet weak var btn_google: UIButton!
    @IBOutlet weak var btn_faceBook: UIButton!
    var tempImgUrl = URL(string:"https://homepages.cae.wisc.edu/~ece533/images/airplane.png")!
    var currentScreenID = kForgotPassword
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCornerRadius()
        self.setConfiguration(screenType: currentScreenID)
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        checkFBLogin()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //MARK: Helper Methods
    func checkGoogleLogin(){
        if GIDSignIn.sharedInstance()?.currentUser != nil{
            
        }else{
            GIDSignIn.sharedInstance()?.signIn()
        }
    }
    
    func checkFBLogin(){
        if let token = AccessToken.current,
            !token.isExpired {
            let token = token.tokenString
            login(with: token)
        }else{
            btn_FBLogin.center = view_faceBook.center
            btn_FBLogin.isHidden = true
            btn_FBLogin.delegate = self
            btn_FBLogin.permissions = ["public_profile","email"]
            view_faceBook.addSubview(btn_FBLogin)
        }
    }
    func setCornerRadius(){
        DispatchQueue.main.async {
            self.btn_SignUp.layer.cornerRadius = self.btn_SignUp.frame.height*0.5
            self.view_Name.addBottomBorderWithColor(color: .gray, width: 0.6)
            self.view_Email.addBottomBorderWithColor(color: .gray, width: 0.6)
            self.view_Password.addBottomBorderWithColor(color: .gray, width: 0.6)
            self.view_google.layer.cornerRadius = self.view_google.frame.height*0.5
            self.view_faceBook.layer.cornerRadius = self.view_faceBook.frame.height*0.5
            
            self.img_google.layer.cornerRadius = self.img_google.frame.height*0.5
            self.img_faceBook.layer.cornerRadius = self.img_faceBook.frame.height*0.5
        }
    }
    func setConfiguration(screenType : String){
        self.btn_ForGotPass.isHidden = (screenType != kSignIn)
        self.view_Name.isHidden = (screenType == kForgotPassword || screenType == kSignIn)
        self.view_Password.isHidden = (screenType == kForgotPassword)
        self.lbl_ScreenTitle.text = (screenType == kForgotPassword) ? kForgotPasswordTitle : ""
        self.btn_SignUp.setTitle((screenType == kSignIn) ? kSignInTitle : (screenType == kSignUP) ? kSignUPTitle : kForgotPasswordTitle, for: .normal)
        self.cnst_ViewHeight = self.cnst_ViewHeight.setMultiplier(multiplier: (screenType == kSignUP) ? 0.39 : (screenType == kSignIn) ? 0.26 : 0.13)
        self.view_SocialLogin.isHidden = !(screenType == kSignUP)
    }

    @IBAction func btn_GoogleSignUpClick(_ sender: Any) {
       self.checkGoogleLogin()
    }
    @IBAction func btn_FBSignUpClick(_ sender: Any) {
       self.btn_faceBook.sendActions(for: .touchUpInside)
    }
    @IBAction func btn_ForgotPress(_ sender: Any) {
        self.setConfiguration(screenType: kForgotPassword)
    }
    @IBAction func btn_BackPress(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btn_SignUpPress(_ sender: UIButton) {
        if (sender.titleLabel?.text == kSignUPTitle){
            printLog(kSignUPTitle)
            self.jumpToController(vControlller:VerifyCodeVC())
        }else if(sender.titleLabel?.text == kSignInTitle){
            printLog(kSignInTitle)
            let userInfo : User_Details = User_Details(user_id: "Ben Beckman",email_id:  "bbeckman@gmail.com",first_Name: "Ben",last_Name: "Beckman",full_Name: "Ben Beckman",img_Url: URL(string: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png")!)
            AppUserdefault.shared.saveStructValueForKey(key: kUserDetailInfo, value: userInfo)

            self.jumpToController(vControlller:IntroductionVC())
        }else{
            printLog(kForgotPasswordTitle)
        }
//        self.showLoaderVC()
//        let data : Data = "&data=imsak1998@gmail.c&password=123456&fcm_token=testtoken".data(using: .utf8)!
//        APILoginModule.emailLogin(by: kServiceUrl+MethodName.LOGIN_WITH_ANY.rawValue, para: data, httpMeethodType: MethodType.POST.rawValue)
    }
    
    
    func login(with token:String){
        let request = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                 parameters: ["fields" : "email,name,first_name,last_name,picture.type(large)"],
                                                 tokenString: token,
                                                 version: nil,
                                                 httpMethod: .get)
        request.start { (connection, result, error) in
            if error != nil{
                print(error?.localizedDescription)
            }
            if let result = result as? NSDictionary {
                printLog("FACEBOOKINFO \(result)")
                
                var userDetailDict = ["userId": result["id"] ?? "",
                                      "emailID" : result["email"] ?? "",
                                      "fullName": result["name"] ?? "",
                                      "firstName": result["first_name"] ?? "",
                                      "lastName": result["last_name"] ?? "",
                                      "imgUrl": ""
                ]
                if let picture = result["picture"] as? NSDictionary, let data = picture["data"] as? NSDictionary, let urlStr = data["url"] as? String{
                    userDetailDict.updateValue(urlStr, forKey: "imgUrl")
                }
                
                let userInfo : User_Details = User_Details(user_id: userDetailDict["userId"] as! String, email_id: userDetailDict["emailID"] as! String, first_Name: userDetailDict["firstName"] as! String, last_Name: userDetailDict["lastName"] as! String, full_Name: userDetailDict["fullName"] as! String, img_Url: URL(string: userDetailDict["imgUrl"] as! String)! )
                AppUserdefault.shared.saveStructValueForKey(key: kUserDetailInfo, value: userInfo)
                
                self.alertViewControllerWith(title: "Successfully Login!", message: "Welcome \(userInfo.full_Name) you logged in Successfully", type: .alert, buttonArryName: ["Ok"]) { (str) in
                    if (str == "Ok"){
                       
                        self.jumpToController(vControlller:IntroductionVC())
                    }
                }
                
                
            }
        }
        
    }
}


extension SignUpVC {
    
    
}

extension SignUpVC : GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let user = user else { return }
        var userInfo : User_Details?
        if user.profile.hasImage
        {
            let dimension = round(100 * UIScreen.main.scale)
            let pic = user.profile.imageURL(withDimension: UInt(dimension))
            if let pic = pic{
                tempImgUrl = pic
            }
            userInfo = User_Details(user_id: user.userID, email_id: user.profile.email, first_Name: user.profile.givenName, last_Name: user.profile.familyName, full_Name: user.profile.name, img_Url: tempImgUrl)
            AppUserdefault.shared.saveStructValueForKey(key: kUserDetailInfo, value: userInfo)
            DispatchQueue.main.async {
                var userStr = "user"
                if let fullName  = userInfo?.full_Name{
                    userStr = fullName
                }
                self.alertViewControllerWith(title: "Successfully Login!", message: "Welcome \(userStr) you logged in Successfully", type: .alert, buttonArryName: ["Ok"]) { (str) in
                    if (str == "Ok"){
                       
                        self.jumpToController(vControlller:IntroductionVC())
                    }
                }
            }
            
        }
        printLog(user.userID,user.profile.familyName,user.profile.givenName)
        printLog("Name : \(user.profile.name ?? "No Name")","Email : \(user.profile.email ?? "No Email")")
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
}



extension SignUpVC : LoginButtonDelegate{
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        let token = result?.token?.tokenString ?? ""
        login(with: token)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
}
