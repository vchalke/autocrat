//
//  SignInOptionsVC.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FBSDKLoginKit
class SignInOptionsVC: MasterVC {
    @IBOutlet weak var imgBase_Google: UIImageView!
    @IBOutlet weak var imgBase_FaceBook: UIImageView!
    @IBOutlet weak var viewBase_Google: UIView!
    @IBOutlet weak var viewBase_FaceBook: UIView!
    @IBOutlet weak var btn_Google: UIButton!
    @IBOutlet weak var btn_FaceBook: UIButton!
    @IBOutlet weak var btn_UsePhones: UIButton!
    let btn_fbLogin = FBLoginButton()
    var imgURL = URL(string:"https://homepages.cae.wisc.edu/~ece533/images/airplane.png")!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setCoreners()
        //For Google Login
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        //For FB Login
        checkFBLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func setCoreners(){
        DispatchQueue.main.async {
            self.viewBase_Google.layer.cornerRadius = self.viewBase_Google.frame.height*0.5
            self.viewBase_FaceBook.layer.cornerRadius = self.viewBase_FaceBook.frame.height*0.5
            
            self.imgBase_Google.layer.cornerRadius = self.viewBase_Google.frame.height*0.5
            self.imgBase_FaceBook.layer.cornerRadius = self.viewBase_FaceBook.frame.height*0.5
            
            self.btn_UsePhones.layer.cornerRadius = self.btn_UsePhones.frame.height*0.5
        }
    }
    @IBAction func btn_GooglePress(_ sender: Any) {
        self.checkGoogleLogin()
    }
    
    @IBAction func btn_FacebookPress(_ sender: Any) {
        self.btn_fbLogin.sendActions(for: .touchUpInside)
    }
    
    @IBAction func btn_phoneMobilePress(_ sender: Any) {
        let viewC = SignUpVC()
        viewC.currentScreenID = kSignUP // kSignUP  kSignIn
        self.jumpToController(vControlller:viewC)
    }
    
    //MARK: Helper Methods
    func checkGoogleLogin(){
        if GIDSignIn.sharedInstance()?.currentUser != nil{
            
        }else{
            GIDSignIn.sharedInstance()?.signIn()
        }
    }
    
    func checkFBLogin(){
        if let token = AccessToken.current,
            !token.isExpired {
            let token = token.tokenString
            login(with: token)
        }else{
            btn_fbLogin.center = viewBase_FaceBook.center
            btn_fbLogin.isHidden = true
            btn_fbLogin.delegate = self
            btn_fbLogin.permissions = ["public_profile","email"]
            viewBase_FaceBook.addSubview(btn_fbLogin)
        }
    }
    
    func login(with token:String){
        let request = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                 parameters: ["fields" : "email,name,first_name,last_name,picture.type(large)"],
                                                 tokenString: token,
                                                 version: nil,
                                                 httpMethod: .get)
        request.start { (connection, result, error) in
            if error != nil{
                print(error?.localizedDescription)
            }
            if let result = result as? NSDictionary {
                printLog("FACEBOOKINFO \(result)")
                
                var userDetailDict = ["userId": result["id"] ?? "",
                                      "emailID" : result["email"] ?? "",
                                      "fullName": result["name"] ?? "",
                                      "firstName": result["first_name"] ?? "",
                                      "lastName": result["last_name"] ?? "",
                                      "imgUrl": ""
                ]
                if let picture = result["picture"] as? NSDictionary, let data = picture["data"] as? NSDictionary, let urlStr = data["url"] as? String{
                    userDetailDict.updateValue(urlStr, forKey: "imgUrl")
                }
                
                let userInfo : User_Details = User_Details(user_id: userDetailDict["userId"] as! String, email_id: userDetailDict["emailID"] as! String, first_Name: userDetailDict["firstName"] as! String, last_Name: userDetailDict["lastName"] as! String, full_Name: userDetailDict["fullName"] as! String, img_Url: URL(string: userDetailDict["imgUrl"] as! String)! )
                AppUserdefault.shared.saveStructValueForKey(key: kUserDetailInfo, value: userInfo)
                
                self.alertViewControllerWith(title: "Successfully Login!", message: "Welcome \(userInfo.full_Name) you logged in Successfully", type: .alert, buttonArryName: ["Ok"]) { (str) in
                    if (str == "Ok"){
                       
                        self.jumpToController(vControlller:IntroductionVC())
                    }
                }
                
                
            }
        }
        
    }
}


extension SignInOptionsVC : GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let user = user else { return }
        var userInfo : User_Details?
        if user.profile.hasImage
        {
            let dimension = round(100 * UIScreen.main.scale)
            let pic = user.profile.imageURL(withDimension: UInt(dimension))
            if let pic = pic{
                imgURL = pic
            }
            userInfo = User_Details(user_id: user.userID, email_id: user.profile.email, first_Name: user.profile.givenName, last_Name: user.profile.familyName, full_Name: user.profile.name, img_Url: imgURL)
            AppUserdefault.shared.saveStructValueForKey(key: kUserDetailInfo, value: userInfo)
            DispatchQueue.main.async {
                var userStr = "user"
                if let fullName  = userInfo?.full_Name{
                    userStr = fullName
                }
                self.alertViewControllerWith(title: "Successfully Login!", message: "Welcome \(userStr) you logged in Successfully", type: .alert, buttonArryName: ["Ok"]) { (str) in
                    if (str == "Ok"){
                        self.jumpToController(vControlller:IntroductionVC())
                    }
                }
            }
            
        }
        printLog(user.userID,user.profile.familyName,user.profile.givenName)
        printLog("Name : \(user.profile.name ?? "No Name")","Email : \(user.profile.email ?? "No Email")")
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
}



extension SignInOptionsVC : LoginButtonDelegate{
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        let token = result?.token?.tokenString ?? ""
        login(with: token)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
}
