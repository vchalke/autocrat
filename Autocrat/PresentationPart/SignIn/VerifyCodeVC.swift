//
//  VerifyCodeVC.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import SGDigitTextField
class VerifyCodeVC: MasterVC {

    @IBOutlet weak var cnst_PinViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnst_PinViewWidth: NSLayoutConstraint!
    @IBOutlet weak var basePinView: UIView!
    @IBOutlet weak var pinTextField: SGDigitTextField!
    let spacing = 15.0
    override func viewDidLoad() {
        super.viewDidLoad()

//        pinTextField.set
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        pinTextField.typingFinishedHandler = { value in
            debugPrint("Value : \(value)")
            if (value.count==4){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                    
                    
                    let userInfo : User_Details = User_Details(user_id: "Ben Beckman",email_id:  "bbeckman@gmail.com",first_Name: "Ben",last_Name: "Beckman",full_Name: "Ben Beckman",img_Url: URL(string: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png")!)
                            
                            AppUserdefault.shared.saveStructValueForKey(key: kUserDetailInfo, value: userInfo)
                            
                           
                    self.jumpToController(vControlller:IntroductionVC())
                }
                
            }
        }
    }

    @IBAction func dismissKeyboard() {
        view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.cnst_PinViewWidth.constant = self.view.frame.width*0.85
        self.cnst_PinViewHeight.constant = (self.cnst_PinViewWidth.constant-(CGFloat(spacing)*3.0))*0.25
        pinTextField.cornerRadius = (self.cnst_PinViewHeight.constant)*0.5
    }

}
