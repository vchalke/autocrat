//
//  AppIconViews.swift
//  Autocrat
//
//  Created by Ios_Team on 08/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class AppIconViews: UIView {

    @IBOutlet var baseView: UIView!
    @IBOutlet var lbl_Title: UILabel!
    @IBOutlet var lbl_SubTitle: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        baseView = loadViewFromNib()
        baseView.frame = bounds
        baseView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        baseView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(baseView)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }

}
