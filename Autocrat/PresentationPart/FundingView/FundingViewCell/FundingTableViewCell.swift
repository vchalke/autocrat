//
//  FundingTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class FundingTableViewCell: MasterTableViewCell {

    @IBOutlet weak var lbl_BankName: UILabel!
    @IBOutlet weak var lbl_duration: UILabel!
    @IBOutlet weak var lbl_prize: UILabel!
    @IBOutlet weak var left_View: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setRounCorners(){
        DispatchQueue.main.async {
        self.left_View.layer.cornerRadius = self.left_View.frame.height*0.38
        }
    }
    
}
