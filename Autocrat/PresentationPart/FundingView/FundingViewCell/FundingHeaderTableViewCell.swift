//
//  FundingHeaderTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class FundingHeaderTableViewCell: MasterTableViewCell {

    @IBOutlet weak var view_Lower: UIView!
    @IBOutlet weak var view_Upper: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setViewCorners(){
        DispatchQueue.main.async {
            self.view_Upper.layer.cornerRadius = self.view_Upper.frame.height*0.5
            self.view_Lower.layer.cornerRadius = self.view_Lower.frame.height*0.5
        }  
    }
    
}
