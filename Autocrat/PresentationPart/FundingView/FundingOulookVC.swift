//
//  FundingOulookVC.swift
//  Autocrat
//
//  Created by Ios_Team on 19/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class FundingOulookVC: RootVC<FundingHeaderTableViewCell,IntroCollectionViewCell,[String],[String]> {
    
    @IBOutlet weak var fundTableView: UITableView!
    let bankNameArr = ["CAPITAL ONE","BANK OF AMERICA","ALLY","CITI BANK","U S BANK"]
    let durationArr = ["74 Months @3.9%","60 Months @2.99%","82 Months @5.94%","74 Months @3.9%","74 Months @3.9%"]
    let rateArr = ["$324.34","$354.34","$334.34","$384.34","$324.34"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initTableViewMultipleCell(tableView: self.fundTableView, baseController: self, arrCellId: ["FundingHeaderTableViewCell","FundingTableViewCell"])
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bankNameArr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FundingTableViewCell", for: indexPath) as! FundingTableViewCell
        cell.setRounCorners()
        cell.lbl_BankName.text = bankNameArr[indexPath.row]
        cell.lbl_duration.text = durationArr[indexPath.row]
        cell.lbl_prize.text = rateArr[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellView = tableView.dequeueReusableCell(withIdentifier: "FundingHeaderTableViewCell") as! FundingHeaderTableViewCell
        cellView.setViewCorners()
        return cellView
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.view.frame.height*0.5
    }
    
}
