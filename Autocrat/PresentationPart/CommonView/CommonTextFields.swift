//
//  CommonTextFields.swift
//  Autocrat
//
//  Created by Ios_Team on 11/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class CommonTextFields: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Error: UILabel!
    @IBOutlet weak var text_Field: UITextField!
    @IBOutlet weak var img_Check: UIImageView!
    @IBOutlet weak var btn_Eye: UIButton!
    @IBOutlet weak var view_Country: UIView!
    @IBOutlet weak var cnst_Country: NSLayoutConstraint!
    @IBOutlet weak var img_Country: UIImageView!
    @IBOutlet weak var lbl_CountryCode: UILabel!
    
    enum Configuration {
        case none
        case empty
        case full
        case error
    }
    
    
    let kCommonTextFields_XIB_NAME = "CommonTextFields"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        Bundle.main.loadNibNamed("CommonTextFields",
                                 owner: self,
                                 options: nil)
        contentView.fixInView(self)
        setup(config: .empty)
        cnst_Country.constant = 0
        view_Country.isHidden = true
        text_Field.font = UIFont(name: "System", size: 18)
        text_Field.attributedPlaceholder = NSAttributedString(string:text_Field.placeholder ?? "", attributes:[NSAttributedString.Key.foregroundColor: UIColor(hex: "808080"), NSAttributedString.Key.font:text_Field.font!]);
    }
    
    func setup(config: Configuration) {
        switch config {
        case .empty:
            lbl_Name.textColor = UIColor(hex: "808080")
            lbl_Error.isHidden = true
            img_Check.isHidden = true
        case .full:
            lbl_Name.textColor = UIColor(hex: "404040")
            lbl_Error.isHidden = true
            img_Check.isHidden = !btn_Eye.isHidden
            img_Check.image = #imageLiteral(resourceName: "checked")
        case .error:
            lbl_Name.textColor = UIColor(hex: "404040")
            lbl_Error.isHidden = false
            img_Check.isHidden = !btn_Eye.isHidden
            img_Check.image = #imageLiteral(resourceName: "cross")
        default:
            break
        }
        lbl_CountryCode.textColor = lbl_Name.textColor
    }
    
    func setup(name: String, placeholder: String) {
        lbl_Name.text = name
        text_Field.placeholder = placeholder
        setup(config: .empty)
    }
    
    func setup(error: String) {
        lbl_Error.text = error
        setup(config: .error)
    }
    
    func setFull() {
        setup(config: .full)
    }
    
    func setupPassword() {
        img_Check.isHidden = true
        btn_Eye.isHidden = false
        text_Field.isSecureTextEntry = true
        btn_Eye.setImage(UIImage(named: "rightCheck"), for: .normal)
        btn_Eye.setImage(UIImage(named: "rightCheck"), for: .selected)
    }
    
    @IBAction func actionPassword(_ sender: UIButton) {
        text_Field.isSecureTextEntry = sender.isSelected
        sender.isSelected = !sender.isSelected
    }
    
    func setupMobile() {
        cnst_Country.constant = 97.0
        view_Country.isHidden = false
        text_Field.keyboardType = .phonePad
    }
    

}

