//
//  ConfirmPayVC.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class ConfirmPayVC: RootVC<ConfirmInfoTableViewCell,IntroCollectionViewCell,[String],[String]> {

    @IBOutlet weak var btn_ProceedPay: UIButton!
    @IBOutlet weak var tbleViewss: UITableView!
    var isFromPaySource = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.tbleViewss, baseController: self, arrCellId: ["ConfirmInfoTableViewCell","PickDropTableViewCell","StartFromTableViewCell","DailNotifTableViewCell"])
    }


    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.btn_ProceedPay.layer.cornerRadius = self.btn_ProceedPay.frame.height*0.5
    }

    @IBAction func btn_BackPress(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func proceedPress(_ sender: Any) {
//        self.setNavigatiobToRight()
//        self.navigationController?.pushViewController(isFromPaySource ? GreatPaySuccessVC() : PaymentSourcesVC(), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmInfoTableViewCell", for: indexPath) as! ConfirmInfoTableViewCell
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PickDropTableViewCell", for: indexPath) as! PickDropTableViewCell
            return cell
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailNotifTableViewCell", for: indexPath) as! DailNotifTableViewCell
        cell.switchView.isEnable = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 300 : 90
    }
}
