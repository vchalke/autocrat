//
//  StartFromTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol StartFromTableViewCellDelegate {
    func txtFieldClicked(index:Int, txtField:UITextField)
}
class StartFromTableViewCell: MasterTableViewCell {
    @IBOutlet weak var lbl_firstTitle: UILabel!
    @IBOutlet weak var lbl_firstDescription: UILabel!
    @IBOutlet weak var txt_firstDesc: UITextField!
    @IBOutlet weak var lbl_secondTitle: UILabel!
    @IBOutlet weak var lbl_secondDescription: UILabel!
    @IBOutlet weak var txt_secondDesc: UITextField!
    @IBOutlet weak var btn_Middle: UIButton!
    var delegate:StartFromTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txt_firstDesc.delegate = self
        self.txt_secondDesc.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension StartFromTableViewCell : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // self.nextTextField(textField: textField)
        return false
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.delegate?.txtFieldClicked(index: textField.tag, txtField: textField)
        printLog("textfielddidbegin\(textField)")
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        printLog("textfildclear\(textField)")
        return true
    }
}
