//
//  DesignDealVC.swift
//  Autocrat
//
//  Created by Ios_Team on 18/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class DesignDealVC: RootVC<PickDropTableViewCell,IntroCollectionViewCell,[String],[String]> {


    @IBOutlet weak var btn_ApplyFilter: UIButton!
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var collectioViews: UICollectionView!
    let topOptionsArr = ["$ 10,000 - 20,000","Audi","BMW","Porsche","$ 5,000 - 10,000","Audi"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetCornersOfButton()
        self.initTableViewMultipleCell(tableView: self.tableViews, baseController: self, arrCellId: ["SelectBudgetMainTableCell","CreateListHeaderTableViewCell"])
        self.initCollectionViewMultipleCell(collectionView: self.collectioViews, baseController: self, arrCellId: ["TopFilterCollectionViewCell"])
        // Do any additional setup after loading the view.
    }
    func SetCornersOfButton(){
        self.btn_ApplyFilter.layer.cornerRadius=self.btn_ApplyFilter.frame.height*0.5
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    @IBAction func back_Pressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func apply_Pressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1}
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectBudgetMainTableCell", for: indexPath) as! SelectBudgetMainTableCell
        cell.setViewsInTable(flag: (indexPath.section==0))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellView = tableView.dequeueReusableCell(withIdentifier: "CreateListHeaderTableViewCell") as! CreateListHeaderTableViewCell
        cellView.lbl_Title.text = "Select Budget"
        return cellView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row==0) ? 250 : 500
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopFilterCollectionViewCell", for: indexPath) as! TopFilterCollectionViewCell
        cell.btn_Prize.setTitle(topOptionsArr[indexPath.row], for: .normal)
        cell.imgViews.image = UIImage.init(named: "cancel")
        cell.hideRight(flag: false)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (indexPath.row == 0) ? self.collectioViews.frame.width*0.5 : self.collectioViews.frame.width*0.3, height: self.collectioViews.frame.height)
    }
}
