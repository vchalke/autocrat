//
//  SelectBudgetMainTableCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class SelectBudgetMainTableCell: RootTableViewCell<SelectBudgetTableViewCell,SelectBudgetCollectionViewCell,[String],[String]> {

    let tabledataArr = ["Below $ 10,000","$ 10,000 - $ 50,000","$ 50,000 - $ 100,000","$ 100,000 - $ 150,000","$ 150,000 - $ 200,000"]
    let cardataArr = ["Audi","BMW","Porsche","Scorpio","Tata","Mahindra","WolksWagon","HondaCity","Scorpio","Tata","Porsche","Scorpio","Tata","Mahindra","WolksWagon","HondaCity"]
    @IBOutlet weak var cnstHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewa: UICollectionView!
    @IBOutlet weak var tableViewsa: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setViewsInTable(flag:Bool){
        self.inTableWithMultipleCollectionCell(collectionView: self.collectionViewa, cellid: "SelectBudgetCollectionViewCell")
        self.inTableWithMultipleTableCell(tableView: self.tableViewsa, cellid: "SelectBudgetTableViewCell")
        self.cnstHeight.constant = flag ? 300 : 1
        self.tableViewsa.isHidden = !flag
        self.collectionViewa.isHidden = flag
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tabledataArr.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectBudgetTableViewCell", for: indexPath) as! SelectBudgetTableViewCell
        cell.lbl_Titles.text = tabledataArr[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardataArr.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectBudgetCollectionViewCell", for: indexPath) as! SelectBudgetCollectionViewCell
        cell.lbl_tiles.text = cardataArr[indexPath.row]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionViewa.frame.width*0.45, height: 40)
    }
}
