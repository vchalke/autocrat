//
//  SelectBudgetTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class SelectBudgetTableViewCell: MasterTableViewCell {

    @IBOutlet weak var btn_Select: UIButton!
    @IBOutlet weak var lbl_Titles: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
