//
//  DelieveryFooterView.swift
//  Autocrat
//
//  Created by Ios_Team on 17/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol DelieveryFooterViewDelegate{
    func bottomOPtionClicked(index:Int)
}
class DelieveryFooterView: UIView {

    @IBOutlet weak var img_FirstView: UIImageView!
     @IBOutlet weak var img_SecondView: UIImageView!
     @IBOutlet weak var img_ThirdView: UIImageView!
     @IBOutlet weak var img_FourView: UIImageView!
     @IBOutlet weak var img_FiveView: UIImageView!
    
     @IBOutlet weak var btn_First: UIButton!
    @IBOutlet weak var btn_Second: UIButton!
    @IBOutlet weak var btn_Third: UIButton!
    @IBOutlet weak var btn_Four: UIButton!
    @IBOutlet weak var btn_Five: UIButton!
    let imageNameArray = ["bagIn","carIn","locationIn","emailIn","menuIn"]
    @IBOutlet weak var baseViews: UIView!
    var delegate : DelieveryFooterViewDelegate?
    var selectedIndex = 0
    var masterVC = MasterVC()
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        baseViews = loadViewFromNib()
        baseViews.frame = bounds
        baseViews.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        baseViews.translatesAutoresizingMaskIntoConstraints = true
        addSubview(baseViews)
        self.loadAllView(forIndex: 2)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }

    func loadAllView(forIndex:Int){
        let btnArry = [self.btn_First,self.btn_Second,self.btn_Third,self.btn_Four,self.btn_Five];
        let imgArry = [self.img_FirstView,self.img_SecondView,self.img_ThirdView,self.img_FourView,self.img_FiveView];
        for i in 0..<btnArry.count {
            let button = btnArry[i]
            button?.addTarget(self, action:#selector(buttonClicked), for: .touchUpInside)
            button?.tag = i
            let img = imgArry[i]
            let appendStr = (i==2) ? "Dark" : "Light" //(i==forIndex)
            img?.image = UIImage.init(named: "\(imageNameArray[i])\(appendStr)")
        }

        
    }
    @objc func buttonClicked(sender:UIButton)
    {
        printLog("hello")
        self.delegate?.bottomOPtionClicked(index: sender.tag)
    }
    
}
