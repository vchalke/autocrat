//
//  ChooseCarTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol ChooseCarTableViewCellDelegate {
    func likeClickedTag(tag:Int, indexPath:NSInteger)
}
class ChooseCarTableViewCell: MasterTableViewCell {

    @IBOutlet weak var car_ImagView: UIImageView!
     @IBOutlet weak var lbl_CarName: UILabel!
     @IBOutlet weak var lbl_CarPrize: UILabel!
    @IBOutlet weak var btn_Like: UIButton!
    @IBOutlet weak var lbl_CarFeature: UILabel!
    var cellIndexPath = 0
    var delegate:ChooseCarTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btn_likePress(_ sender: UIButton) {
        printLog(sender.tag)
        self.delegate?.likeClickedTag(tag: sender.tag, indexPath: cellIndexPath)
        
    }
}
