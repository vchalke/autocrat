//
//  ChooseCarDetailVC.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class ChooseCarDetailVC:  RootVC<ChooseCarTableViewCell,SlidingCollectionViewCell,[String],[String]> {

    let titleArr = ["All Car","Sedan","Hatchback","Pick Up Truck","BMW","Mercedies","Audy","All Car","Sedan","Hatchback"]
    let carNamePrizeArr = [["BMW 3 Series","$ 123 / Month"],["Datsun Go +","$ 98 / Month"],["BMW i8 Roadstar","$ 128 / Month"],["BMW i8 Roadstar","$ 398 / Month"],["BMW 3 Series","$ 123 / Month"],["Datsun Go +","$ 98 / Month"],["BMW i8 Roadstar","$ 398 / Month"],["BMW 3 Series","$ 123 / Month"],["BMW 3 Series","$ 123 / Month"],["BMW 3 Series","$ 123 / Month"]]
    var likeTagArr = [1,0,1,0,1,0,1,0,1,0]
    @IBOutlet weak var optionCollectionView: UICollectionView!
    @IBOutlet weak var table_Views: UITableView!
    var selectCell = 0
    var isFromSearch = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.table_Views, baseController: self, arrCellId: ["ChooseCarTableViewCell"])
        self.initCollectionViewMultipleCell(collectionView: self.optionCollectionView, baseController: self, arrCellId: ["SlidingCollectionViewCell"])
    }

    @IBAction func backPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filter_Pressed(_ sender: Any) {
        self.setNavigatiobToRight()
        self.navigationController?.pushViewController(DesignDealVC(), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carNamePrizeArr.count
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.jumpToController(vControlller: VehicleDetailPageVC())
        self.jumpToController(vControlller: isFromSearch ? VehicleDetailPageVC() : ConfirmSubmissionVC())
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseCarTableViewCell", for: indexPath) as! ChooseCarTableViewCell
//        cell.car_ImagView.image = UIImage.init(named: "CarImg\(indexPath.row)")
        cell.car_ImagView.image = UIImage.init(named: "ChooseCarIcon\(indexPath.row%4)")
        cell.lbl_CarName.text = carNamePrizeArr[indexPath.row][0]
        cell.lbl_CarPrize.text = carNamePrizeArr[indexPath.row][1]
        cell.btn_Like.setImage(UIImage.init(named: (likeTagArr[indexPath.row] == 0) ? "likeSelect" : "likeNoSelect"), for: .normal)
        cell.btn_Like.tag = likeTagArr[indexPath.row]
        cell.btn_Like.isHidden = isFromSearch
        cell.cellIndexPath = indexPath.row
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlidingCollectionViewCell", for: indexPath) as! SlidingCollectionViewCell
        cell.lbl_Title.text = titleArr[indexPath.row]
        cell.lbl_Base.isHidden = (selectCell != indexPath.row)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            selectCell = indexPath.row
            DispatchQueue.main.async {
                self.optionCollectionView.scrollToItem(at: IndexPath(item: indexPath.row, section: 0), at: .centeredHorizontally, animated: true)
                self.optionCollectionView.reloadData()
            }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.optionCollectionView.frame.width*0.3, height: self.optionCollectionView.frame.height)
    }
    
}

extension ChooseCarDetailVC : ChooseCarTableViewCellDelegate{
    
    func likeClickedTag(tag: Int, indexPath: NSInteger) {
        printLog("TAG ARR BEFORE \(likeTagArr)")
        likeTagArr[indexPath] = (tag == 0) ? 1 : 0;
        printLog("TAG ARR AFTER \(likeTagArr)")
        DispatchQueue.main.async {
            self.table_Views.reloadData()
        }
    }
}
