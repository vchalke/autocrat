//
//  NewsReviewsVC.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class NewsReviewsVC: RootVC<PickDropTableViewCell,SlidingCollectionViewCell,[String],[String]>  {

    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var tableaViews: UITableView!
    var selectedCell = 0
    let titleArrC = ["Overview","Gallery","Variants","Specifications","Overview","Variants","Specifications","Gallery","Overview","Gallery"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTableViewMultipleCell(tableView: self.tableaViews, baseController: self, arrCellId: ["MostReadTableViewCell","TopStoriesTableViewCell","BestModifyTableViewCell"])
        self.initCollectionViewMultipleCell(collectionView: self.topCollectionView, baseController: self, arrCellId: ["SlidingCollectionViewCell"])
    }

    @IBAction func btn_BackPress(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "MostReadTableViewCell", for: indexPath) as! MostReadTableViewCell
            cell.setCellInView()
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopStoriesTableViewCell", for: indexPath) as! TopStoriesTableViewCell
            cell.setTableViewCellInView()
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "BestModifyTableViewCell", for: indexPath) as! BestModifyTableViewCell
        cell.setTableViewInView()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 360 : (indexPath.row == 1) ? 220 : 500
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArrC.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlidingCollectionViewCell", for: indexPath) as! SlidingCollectionViewCell
        cell.lbl_Title.text = titleArrC[indexPath.row]
        cell.lbl_Base.isHidden = (selectedCell != indexPath.row)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            selectedCell = indexPath.row
            DispatchQueue.main.async {
                self.topCollectionView.scrollToItem(at: IndexPath(item: indexPath.row, section: 0), at: .centeredHorizontally, animated: true)
                self.topCollectionView.reloadData()
            }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.topCollectionView.frame.width*0.3, height: self.topCollectionView.frame.height)
    }
    

}
