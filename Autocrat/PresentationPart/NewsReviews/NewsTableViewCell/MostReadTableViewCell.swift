//
//  MostReadTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class MostReadTableViewCell: RootTableViewCell<PickDropTableViewCell,MostReadCollectionViewCell,[String],[String]> {

//    @IBOutlet weak var mostReadCollectionView: UICollectionView!
    
    @IBOutlet weak var mstReadCollView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellInView(){
        self.inTableWithMultipleCollectionCell(collectionView : self.mstReadCollView,cellid:"MostReadCollectionViewCell")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MostReadCollectionViewCell", for: indexPath) as! MostReadCollectionViewCell
        cell.setDataOfUser()
                return cell
        
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.mstReadCollView.frame.width*0.7, height: self.mstReadCollView.frame.height*0.98)
    }
    
}
