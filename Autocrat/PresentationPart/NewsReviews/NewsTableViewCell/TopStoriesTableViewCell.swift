//
//  TopStoriesTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class TopStoriesTableViewCell: RootTableViewCell<PickDropTableViewCell,TopStoryCollectionViewCell,[String],[String]>  {

    @IBOutlet weak var topStoryCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTableViewCellInView(){
        self.inTableWithMultipleCollectionCell(collectionView : self.topStoryCollectionView,cellid:"TopStoryCollectionViewCell")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopStoryCollectionViewCell", for: indexPath) as! TopStoryCollectionViewCell
        cell.setDataOfUser()
        return cell
        
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.topStoryCollectionView.frame.width*0.25, height: self.topStoryCollectionView.frame.width*0.3)
    }
    
}
