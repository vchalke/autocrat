//
//  BestModifyTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class BestModifyTableViewCell: RootTableViewCell<PickDropTableViewCell,BestModifyCollectionViewCell,[String],[String]> {

    @IBOutlet weak var bestModifyCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTableViewInView(){
        self.inTableWithMultipleCollectionCell(collectionView : self.bestModifyCollectionView,cellid:"BestModifyCollectionViewCell")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestModifyCollectionViewCell", for: indexPath) as! BestModifyCollectionViewCell
        cell.setRadiusOfImg()
        return cell
        
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.bestModifyCollectionView.frame.width, height: 120)
    }
}
