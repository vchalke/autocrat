//
//  BestModifyCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class BestModifyCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var img_Users: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_userBirth: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setRadiusOfImg(){
        DispatchQueue.main.async {
            self.img_Users.layer.cornerRadius = self.img_Users.frame.height*0.5
        }
    }
}
