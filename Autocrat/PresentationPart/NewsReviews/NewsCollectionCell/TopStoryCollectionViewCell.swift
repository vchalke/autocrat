//
//  TopStoryCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class TopStoryCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var img_User: UIImageView!
    @IBOutlet weak var lbl_UserName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setDataOfUser(){
        DispatchQueue.main.async {
            self.img_User.layer.cornerRadius = self.img_User.frame.height*0.5
        }
        
    }

}
