//
//  MostReadCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 17/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class MostReadCollectionViewCell: MasterCollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var user_Birth: UILabel!
    @IBOutlet weak var user_Name: UILabel!
    @IBOutlet weak var user_Image: UIImageView!
    
    func setDataOfUser(){
        DispatchQueue.main.async {
            self.user_Image.layer.cornerRadius = self.user_Image.frame.height*0.5
        }
        
    }

}
