//
//  DealHubTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 29/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol DealHubTableViewCellDelegate {
    func viewOfferClicked()
    func acceptOfferClicked()
}
class DealHubTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_camera: UIButton!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var lbl_Left: UILabel!
    @IBOutlet weak var lbl_Right: UILabel!
    @IBOutlet weak var btn_leftOne: UIButton!
    @IBOutlet weak var btn_RightOne: UIButton!
     var delegate: DealHubTableViewCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btn_LeftPressed(_ sender: Any) {
        self.delegate.viewOfferClicked()
    }
    @IBAction func btn_RightPressed(_ sender: Any) {
        self.delegate.acceptOfferClicked()
    }
    func setDataOnIndex(index:Int){
        self.btn_camera.isHidden = (index != 0)
        self.leftView.backgroundColor = UIColor.colorWithHexString(hex: (index%2 == 0) ? "#05A8FC" : "#003366")
        self.rightView.backgroundColor = UIColor.colorWithHexString(hex: (index%2 == 0) ? "#34B559" : "#CCCCCC")
        self.lbl_Left.text = (index%2 == 0) ? "VIEW OFFER" : "AWAITING RESPONSE"
        self.lbl_Right.text = (index%2 == 0) ? "ACCEPT OFFER" : "ACCEPT OFFER"
        
    }
}



// Light Blue Colour = 05A8FC
// Light Green Colour = 34B559
// Dark Green Colour = 003366
// Light Grey Colour = CCCCCC
