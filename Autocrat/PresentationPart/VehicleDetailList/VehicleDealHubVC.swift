//
//  VehicleDealHubVC.swift
//  Autocrat
//
//  Created by Ios_Team on 29/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class VehicleDealHubVC: RootVC<PickDropTableViewCell,SlidingCollectionViewCell,[String],[String]>  {

    @IBOutlet weak var topImageBaseView: UIView!
    @IBOutlet weak var dealHubLstTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.dealHubLstTableView, baseController: self, arrCellId: ["DealHubTableViewCell"])
        self.setCorenerRadius()
    }


    func setCorenerRadius(){
        self.topImageBaseView.layer.cornerRadius = self.topImageBaseView.frame.height*0.5
    }
    
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 6
       }
       
       override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
               let cell = tableView.dequeueReusableCell(withIdentifier: "DealHubTableViewCell", for: indexPath) as! DealHubTableViewCell
        cell.setDataOnIndex(index: indexPath.row)
        cell.delegate = self
               return cell
           
       }
       
       override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           
               return 340
       }
}

extension VehicleDealHubVC : DealHubTableViewCellDelegate{
    func viewOfferClicked() {
        
    }
    
    func acceptOfferClicked() {
        self.jumpToController(vControlller: ConfirmPayVC())
    }
    
    
}
