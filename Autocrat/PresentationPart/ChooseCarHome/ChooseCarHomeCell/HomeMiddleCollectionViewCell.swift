//
//  HomeMiddleCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 13/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class HomeMiddleCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var gradientView: Gradient!
    @IBOutlet weak var lbl_Number: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_SubTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setCornerRadii(){
        DispatchQueue.main.async {
            self.lbl_Number.layer.cornerRadius = self.lbl_Number.frame.height*0.5
        }
    }

}
