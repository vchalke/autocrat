//
//  ChooseCarHomeVC.swift
//  Autocrat
//
//  Created by Ios_Team on 12/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import SwiftCSV
import CSV

class ChooseCarHomeVC: RootVC<ProfileTableViewCell,IntroCollectionViewCell,[String],[String]> {

    @IBOutlet weak var baseViewTxt: UIView!
    @IBOutlet weak var txt_Search: UITextField!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var upperCollectionView: UICollectionView!
    @IBOutlet weak var middleCollectionView: UICollectionView!
    @IBOutlet weak var bottomCollectionView: UICollectionView!
    let optionsArr = ["New Car","Used Car","Subscriptions","Trade Values","Compare Deals","News"]
    let optionsSubArr = ["Latest Makes & Models","Choose Car","Classis Car","Trade Values","Compare Deals","News"]
    let colourArr :[UIColor] = [#colorLiteral(red: 1, green: 0.7194030112, blue: 0, alpha: 1) ,#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1) , #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1) , #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1) , #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) , #colorLiteral(red: 0.9176470588, green: 0.5137254902, blue: 0, alpha: 1)]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.txt_Search.delegate = self
        pgControl.numberOfPages = 5
        self.initCollectionViewMultipleCell(collectionView: self.upperCollectionView, baseController: self, arrCellId: ["IntroCollectionViewCell"])
        self.initCollectionViewMultipleCell(collectionView: self.middleCollectionView, baseController: self, arrCellId: ["HomeMiddleCollectionViewCell"])
        self.initCollectionViewMultipleCell(collectionView: self.bottomCollectionView, baseController: self, arrCellId: ["IntroCollectionViewCell"])
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    
    @IBAction func btn_MenuPress(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.pushViewController(ProfileVC(), animated: true)
    }
    @IBAction func btn_QualificationPress(_ sender: Any) {
        AppUserdefault.shared.saveBoolValueForKey(key: kIsFromHomeVC, value: true)
        self.navigationController?.pushViewController(WelcomeProfessionScrVC(), animated: true)
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (collectionView == self.upperCollectionView) ? 5 : (collectionView == self.upperCollectionView) ? optionsArr.count : 4
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == self.upperCollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
            cell.imgViews.image = UIImage.init(named: "HomeCarUp\(indexPath.row)")
                    return cell
        } else if (collectionView == self.middleCollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeMiddleCollectionViewCell", for: indexPath) as! HomeMiddleCollectionViewCell
            cell.setCornerRadii()
            cell.lbl_Number.text = "\(indexPath.row+1)"
            cell.lbl_Title.text = optionsArr[indexPath.row]
            cell.lbl_SubTitle.text = optionsSubArr[indexPath.row]
            cell.gradientView.startColor = colourArr[indexPath.row]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
//                cell.imgViews.image = UIImage.init(named: "HomeCarBottom\(indexPath.row)")
        cell.imgViews.image = UIImage.init(named: "NewsReviews1")
        
        cell.setCornerTo(radius:10.0)
                return cell
        
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView == self.upperCollectionView) ? self.upperCollectionView.frame.width : (collectionView == self.middleCollectionView) ? self.middleCollectionView.frame.width * 0.42 : self.bottomCollectionView.frame.width*0.7
        let height = (collectionView == self.upperCollectionView) ? self.upperCollectionView.frame.height : (collectionView == self.middleCollectionView) ? self.middleCollectionView.frame.height*0.7 : self.bottomCollectionView.frame.height*0.9
        return CGSize(width: width, height: height)
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == self.upperCollectionView){
            printLog("page indexPath of upperCollectionView = \(indexPath.row)")
        }else if (collectionView == self.middleCollectionView){
            printLog("page indexPath of middleCollectionView = \(indexPath.row)")
            let vc = ChooseCarDetailVC()
            vc.isFromSearch = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            printLog("page indexPath of bottomCollectionView = \(indexPath.row)")
        }
        
    }
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        printLog("page = \(page)")
        self.pgControl.currentPage = page
    }

}

extension ChooseCarHomeVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == self.txt_Search){
            self.view.endEditing(true)
            self.setNavigatiobToRight()
            self.navigationController?.pushViewController(CreateListingVC(), animated: true)
        }
    }
}



/*

     func readCSVTwo(){
         
 //        let stream = InputStream(fileAtPath: "/Users/os/Desktop/Company_Project/ClientApp/Autocrat/Autocrat/CSVFiles/AddressLiscence_copy.csv")!
 //        let csvData = try! CSVReader(stream: stream,
 //                                     codecType: UTF16.self,
 //                                     endian: .big)
 //
 //        let headerRow = csvData.headerRow
 //        printLog("csvData headerRow \(headerRow)")
 //        let currentRow = csvData.currentRow
 //        printLog("csvData currentRow \(currentRow)")
         
         guard let csvPath = Bundle.main.path(forResource: "AddressLiscence_copy", ofType: "csv") else { return }
         do {
             let csvData = try String(contentsOfFile: csvPath, encoding: String.Encoding.utf8)
             printLog("csvData String \(csvData)")
             let csv = try CSV(string: csvData)
             printLog("csv.header \(csv.header)")
             printLog("csv.enumeratedRows \(csv.enumeratedRows)")
             printLog("csv.namedRows \(csv.namedRows )")
             printLog("csv.namedColumns \(csv.namedColumns)")
             
             let csvString = csvData
             let csvDataOne = try! CSVReader(string: csvString,
                                      hasHeaderRow: true) // It must be true.

             let csvFilterd = csvDataOne.filter { (array) -> Bool in
                 return (array.contains("Vijay"))
             }
             printLog("\(csvFilterd)")
             
             while csvDataOne.next() != nil {
                
                 printLog("\(csvDataOne["first_name"]!)")   // => "1"
                 printLog("\(csvDataOne["last_name"]!)") // => "foo"
             }
     
             let stream = OutputStream(toFileAtPath: "/Users/os/Desktop/Company_Project/ClientApp/Autocrat/Autocrat/CSVFiles/AddressLiscence_copy.csv", append: false)!
             let csvWrite = try! CSVWriter(stream: stream)
             for data in csv.enumeratedRows{
                 try! csvWrite.write(row: data)
             }
             try! csvWrite.write(row: ["Vijay", "Chalke","Vikhroli", "0124454","9874563210"])
             csvWrite.stream.close()
             
             let csvDataTwo = try String(contentsOfFile: csvPath, encoding: String.Encoding.utf8)
             printLog("csvDataTwo String \(csvDataTwo)")
             let csvTwo = try CSV(string: csvDataTwo)
             printLog("csvTwo.header \(csvTwo.header)")
             printLog("csvTwo.enumeratedRows \(csvTwo.enumeratedRows)")
             printLog("csvTwo.namedRows \(csvTwo.namedRows )")
             printLog("csvTwo.namedColumns \(csvTwo.namedColumns)")
             
             
 //            let csvDataAfter = try String(contentsOfFile: csvPath, encoding: String.Encoding.utf8)
 //            let csvDataOneAfter = try! CSVReader(string: csvDataAfter,
 //                                            hasHeaderRow: true) // It must be true.
 //
 //            while csvDataOneAfter.next() != nil {
 //                printLog("\(csvDataOne["first_name"]!)")   // => "1"
 //                printLog("\(csvDataOne["last_name"]!)") // => "foo"
 //            }
             
             
         } catch{
             printLog(error)
         }
         
     }
 

 func readCSVOne(){
         guard let csvPath = Bundle.main.path(forResource: "AddressLiscence_copy", ofType: "csv") else { return }
         do {
             let csvData = try String(contentsOfFile: csvPath, encoding: String.Encoding.utf8)
             printLog("csvData String \(csvData)")
             let csv = csvRows(data: csvData)
             for row in csv {
                 printLog(row)
             }
             
 //            let csvURL = NSURL(string: "AddressLiscence.csv")!
 //            var error: NSErrorPointer = nil
 //            let csvFile = try CSV(url: csvURL as URL)

 //            let csvFile : CSV = try CSV(url: URL(fileURLWithPath: "/Users/os/Desktop/Company_Project/ClientApp/Autocrat/Autocrat/CSVFiles/AddressLiscence_copy.csv"))
 //            // Rows
 //            let rows = csvFile.namedRows
 //            printLog(rows)
 //            let headers = csvFile.header  //=> ["id", "name", "age"]
 //            printLog(headers)
 //            let alice = csvFile.namedRows[0]    //=> ["id": "1", "name": "Alice", "age": "18"]
 //            printLog(alice)
 //            let bob = csvFile.namedRows[1]      //=> ["id": "2", "name": "Bob", "age": "19"]
 //            printLog(bob)
 //            // Columns
 //            let columns = csvFile.namedColumns
 //            printLog(columns)
 //            let names = csvFile.namedColumns["address"]  //=> ["Alice", "Bob", "Charlie"]
 //            printLog(names!)
 //            let ages = csvFile.namedColumns["age"]
 //            printLog(ages!)
             
         } catch{
             printLog(error)
         }
     }
 
 func csvRows(data: String) -> [[String]] {
     var result: [[String]] = []
     let rows = data.components(separatedBy: "\n")
     for row in rows {
         let columns = row.components(separatedBy: ";")
         result.append(columns)
     }
     return result
 }
 
 */
