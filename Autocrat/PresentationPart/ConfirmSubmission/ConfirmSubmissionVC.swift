//
//  ConfirmSubmissionVC.swift
//  Autocrat
//
//  Created by Ios_Team on 28/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class ConfirmSubmissionVC: RootVC<PickDropTableViewCell,ConfSubmissionCollectionViewCell,[String],[String]>  {

    @IBOutlet weak var collectViews: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initCollectionViewMultipleCell(collectionView: self.collectViews, baseController: self, arrCellId: ["ConfSubmissionCollectionViewCell"])
        
    }
    @IBAction func btn_backPressed(_ sender: Any) {
        self.backToPreviousController()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConfSubmissionCollectionViewCell", for: indexPath) as! ConfSubmissionCollectionViewCell
        cell.vContr = self
        cell.setUpTableViewData()
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            DispatchQueue.main.async {
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectViews.frame.width*0.95, height: self.collectViews.frame.height)
    }

}
