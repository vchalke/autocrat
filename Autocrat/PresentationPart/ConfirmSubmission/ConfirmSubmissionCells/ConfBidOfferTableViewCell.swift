//
//  ConfBidOfferTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 28/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol ConfBidOfferTableViewCellDelegate {
    func bidValuesDropClicked()
}
class ConfBidOfferTableViewCell: UITableViewCell {

    @IBOutlet weak var cnst_HeightOfBidViews: NSLayoutConstraint!
    @IBOutlet weak var bidBaseView: UIView!
    @IBOutlet weak var btn_DropDown: UIButton!
    var delegate: ConfBidOfferTableViewCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btn_DropClicked(_ sender: Any) {
        self.delegate.bidValuesDropClicked()
    }
    
}
