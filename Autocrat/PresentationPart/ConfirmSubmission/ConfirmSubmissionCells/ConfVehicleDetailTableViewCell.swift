//
//  ConfVehicleDetailTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 28/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol ConfVehicleDetailTableViewCellDelegate {
    func locationDropClicked(flag:Bool)
}
class ConfVehicleDetailTableViewCell: UITableViewCell {

   @IBOutlet weak var cnst_HeightOfInfoBaseView: NSLayoutConstraint!
        @IBOutlet weak var detailInfoBaseView: UIView!
        @IBOutlet weak var btn_DropDown: UIButton!
        var delegate: ConfVehicleDetailTableViewCellDelegate!
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }

        var isShowDetailInfo = false {
            didSet {
                self.cnst_HeightOfInfoBaseView.constant = isShowDetailInfo ? 200 : 0
            }
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
        @IBAction func btn_DropClicked(_ sender: Any) {
            self.delegate.locationDropClicked(flag: !isShowDetailInfo)
        }
}
