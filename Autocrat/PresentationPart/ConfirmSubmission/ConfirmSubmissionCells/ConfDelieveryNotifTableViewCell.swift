//
//  ConfDelieveryNotifTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 28/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import LabelSwitch
protocol ConfDelieveryNotifTableViewCellDelegate {
    func confirmSubmission()
}
class ConfDelieveryNotifTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_Confirm: UIButton!
    @IBOutlet weak var `switch`: LabelSwitch!
     var delegate: ConfDelieveryNotifTableViewCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btn_ConfirmPressed(_ sender: Any) {
        self.delegate.confirmSubmission()
    }
}
