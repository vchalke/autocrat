//
//  ConfAddOnsTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 28/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
protocol ConfAddOnsTableViewCellDelegate {
    func plusButtonClicked()
}
class ConfAddOnsTableViewCell: UITableViewCell {


        @IBOutlet weak var cnst_HeightOfAddOnsViews: NSLayoutConstraint!
        @IBOutlet weak var addOnBaseView: UIView!
        @IBOutlet weak var btn_Plus: UIButton!
        var delegate: ConfAddOnsTableViewCellDelegate!
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
        @IBAction func btn_PlusClicked(_ sender: Any) {
            self.delegate.plusButtonClicked()
        }
        
    }
