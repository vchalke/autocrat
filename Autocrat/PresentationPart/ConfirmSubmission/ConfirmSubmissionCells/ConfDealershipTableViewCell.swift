//
//  ConfDealershipTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 28/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import MapKit
protocol ConfDealershipTableViewCellDelegate {
    func locationDropClicked(flag:Bool)
}
class ConfDealershipTableViewCell: UITableViewCell {

    @IBOutlet weak var mapKitView: MKMapView!
    @IBOutlet weak var cnst_HeightOfmapView: NSLayoutConstraint!
    @IBOutlet weak var mapBaseView: UIView!
    @IBOutlet weak var btn_DropDown: UIButton!
//    var isShowLocation = false
    var delegate: ConfDealershipTableViewCellDelegate!
    var pointAnnotation:CustomPointAnnotation!
    let locationManager = CLLocationManager()
    var pinAnnotationView:MKPinAnnotationView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var isShowLocation = false {
        didSet {
            self.cnst_HeightOfmapView.constant = isShowLocation ? 200 : 0
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btn_DropClicked(_ sender: Any) {
        self.delegate.locationDropClicked(flag: !isShowLocation)
    }
    
    func setMapView(){
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapKitView.showsUserLocation = true
        self.mapKitView.delegate = self
    }
    
    
}

extension ConfDealershipTableViewCell : MKMapViewDelegate, CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        self.mapKitView.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
//        for i in 0..<lattitudeArr.count {
//            let center = CLLocationCoordinate2D(latitude: lattitudeArr[i], longitude: longitudeArr[i])
//            let regionO = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
//            self.mapViews.setRegion(regionO, animated: true)
//        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let lattitude = "\(annotation.coordinate.latitude)"
        let reuseIdentifier = "locationPin_\(lattitude)"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "locationPin.png")
        
        return annotationView
    }
}
