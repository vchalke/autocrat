//
//  ConfSubmissionCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 28/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class ConfSubmissionCollectionViewCell: MasterCollectionViewCell,UITableViewDelegate,UITableViewDataSource {
//class ConfSubmissionCollectionViewCell:  RootTableViewCell<PickDropTableViewCell,BestModifyCollectionViewCell,[String],[String]>{
    @IBOutlet weak var tableVIews: UITableView!
    var isShowLocation = false
    var vContr : UIViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpTableViewData(){
        let cellarr = ["ConfVehicleDetailTableViewCell","ConfDealershipTableViewCell","ConfBidOfferTableViewCell","ConfAddOnsTableViewCell","ConfBidStartEndTableViewCell","ConfDelieveryNotifTableViewCell"]
        self.tableVIews?.dataSource = self
        self.tableVIews?.delegate = self
        cellarr.forEach { (cellIdentifier) in
            self.tableVIews?.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        }
        
        
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfVehicleDetailTableViewCell", for: indexPath) as! ConfVehicleDetailTableViewCell
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfDealershipTableViewCell", for: indexPath) as! ConfDealershipTableViewCell
            cell.isShowLocation = isShowLocation
            cell.setMapView()
            cell.delegate = self
            return cell
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfBidOfferTableViewCell", for: indexPath) as! ConfBidOfferTableViewCell
            return cell
        }else if(indexPath.row == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfAddOnsTableViewCell", for: indexPath) as! ConfAddOnsTableViewCell
            return cell
        }else if(indexPath.row == 4){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfBidStartEndTableViewCell", for: indexPath) as! ConfBidStartEndTableViewCell
            return cell
        }else if(indexPath.row == 5){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfDelieveryNotifTableViewCell", for: indexPath) as! ConfDelieveryNotifTableViewCell
            cell.switch.isEnable = true
            cell.delegate = self
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "BestModifyTableViewCell", for: indexPath) as! BestModifyTableViewCell
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0){
            return 300
        }else if(indexPath.row == 1){
            return isShowLocation ? 275 : 75
        }else if(indexPath.row == 2){
            return 75
        }else if(indexPath.row == 3){
            return 75
        }else if(indexPath.row == 4){
            return 75
        }else if(indexPath.row == 5){
            return 150
        }
        return 75
    }
    
    func setFlagOfView(){
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}

extension ConfSubmissionCollectionViewCell : ConfDealershipTableViewCellDelegate{
    func locationDropClicked(flag:Bool) {
        isShowLocation = flag
        self.tableVIews.reloadData()
    }
    
    
}
extension ConfSubmissionCollectionViewCell : ConfDelieveryNotifTableViewCellDelegate{
    func confirmSubmission() {
        vContr.navigationController?.pushViewController(VehicleDealHubVC(), animated: true)
    }
    
    
    
    
}
