//
//  TimeValueCollectionViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class TimeValueCollectionViewCell: MasterCollectionViewCell {

    @IBOutlet weak var baseLblView: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCornerOfView(index : NSInteger){
        DispatchQueue.main.async {
            self.baseLblView.layer.cornerRadius = self.baseLblView.frame.height*0.5
        }
        self.baseLblView.backgroundColor = UIColor.colorWithHexString(hex: (index == 0) ? "285BD4" : "D1D1D6")
        self.lbl_Title.textColor = UIColor.colorWithHexString(hex: (index == 0) ? "FFFFFF" : "1F2124")
    }
}
