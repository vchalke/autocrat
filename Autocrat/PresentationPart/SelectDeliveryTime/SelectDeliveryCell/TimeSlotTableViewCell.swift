//
//  TimeSlotTableViewCell.swift
//  Autocrat
//
//  Created by Ios_Team on 19/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class TimeSlotTableViewCell: RootTableViewCell<PickDropTableViewCell,TimeValueCollectionViewCell,[String],[String]> {

    let timeArr = ["10.30 AM","11.30 AM","12.30 PM","01.30 PM","02.30 PM","03.30 PM","04.30 PM","05.30 PM","06.30 PM"]
    @IBOutlet weak var collectionTwo: UICollectionView!
    @IBOutlet weak var collectionOne: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTableViewData(){
        self.inTableWithMultipleCollectionCell(collectionView : self.collectionOne,cellid:"TimeValueCollectionViewCell")
        self.inTableWithMultipleCollectionCell(collectionView : self.collectionTwo,cellid:"TimeValueCollectionViewCell")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeArr.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeValueCollectionViewCell", for: indexPath) as! TimeValueCollectionViewCell
        cell.lbl_Title.text = timeArr[indexPath.row]
        cell.setCornerOfView(index: indexPath.row)
        return cell
        
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == self.collectionOne){
            return CGSize(width: self.collectionTwo.frame.width*0.3, height: self.collectionTwo.frame.height)
        }
        return CGSize(width: self.collectionTwo.frame.width*0.3, height: self.collectionTwo.frame.height)
    }
}
