//
//  SelectDeliveryVC.swift
//  Autocrat
//
//  Created by Ios_Team on 18/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class SelectDeliveryVC: RootVC<PickDropTableViewCell,SlidingCollectionViewCell,[String],[String]> {

    @IBOutlet weak var tableViews: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.tableViews, baseController: self, arrCellId: ["TimeSlotTableViewCell","CalenderTableViewCell","StartFromTableViewCell"])
    }


    @IBAction func btn_BackPress(_ sender: Any) {
        self.backToPreviousController()
    }
    
    @IBAction func btn_goPress(_ sender: Any) {
        self.jumpToController(vControlller: DelieveryDetailsVC())
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row==0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
        return cell
        }else if (indexPath.row==1){
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalenderTableViewCell", for: indexPath) as! CalenderTableViewCell
        return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeSlotTableViewCell", for: indexPath) as! TimeSlotTableViewCell
        cell.setTableViewData()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row==0) ? 80 : (indexPath.row==1) ? 370 : 180
    }
    
}
