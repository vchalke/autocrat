//
//  CarOverviewVC.swift
//  Autocrat
//
//  Created by Ios_Team on 18/11/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class CarOverviewVC: RootVC<OverviewMainTableViewCell,SlidingCollectionViewCell,[String],[String]> {

    @IBOutlet weak var tableVieww: UITableView!
    @IBOutlet weak var topCollectionView: UICollectionView!
    let titleArrs = ["Overview","Gallery","Variants","Specifications","Overview","Gallery","Variants","Specifications"]
    var selectedCell = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTableViewMultipleCell(tableView: self.tableVieww, baseController: self, arrCellId: ["OverviewMainTableViewCell","DescriptionTableViewCell"])
        self.initCollectionViewMultipleCell(collectionView: self.topCollectionView, baseController: self, arrCellId: ["SlidingCollectionViewCell"])

    }

    @IBAction func btn_BackPress(_ sender: Any) {
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row==0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewMainTableViewCell", for: indexPath) as! OverviewMainTableViewCell
        return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionTableViewCell", for: indexPath) as! DescriptionTableViewCell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row==0) ? 500 : 300
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArrs.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlidingCollectionViewCell", for: indexPath) as! SlidingCollectionViewCell
        cell.lbl_Title.text = titleArrs[indexPath.row]
        cell.lbl_Base.isHidden = (selectedCell != indexPath.row)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            selectedCell = indexPath.row
            DispatchQueue.main.async {
                self.topCollectionView.scrollToItem(at: IndexPath(item: indexPath.row, section: 0), at: .centeredHorizontally, animated: true)
                self.topCollectionView.reloadData()
            }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.topCollectionView.frame.width*0.3, height: self.topCollectionView.frame.height)
    }

}
