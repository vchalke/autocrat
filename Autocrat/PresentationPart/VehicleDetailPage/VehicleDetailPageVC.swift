//
//  VehicleDetailPageVC.swift
//  Autocrat
//
//  Created by Ios_Team on 15/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit

class VehicleDetailPageVC: RootVC<ConfirmInfoTableViewCell,IntroCollectionViewCell,[String],[String]>  {

    @IBOutlet weak var detailTableViews: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.detailTableViews, baseController: self, arrCellId: ["ConfirmInfoTableViewCell","PickDropTableViewCell","StartFromTableViewCell","DailNotifTableViewCell"])
    }


    @IBAction func backPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func locatePressed(_ sender: Any) {
        self.setNavigatiobToRight()
        self.navigationController?.pushViewController(LocateVehicleVC(), animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmInfoTableViewCell", for: indexPath) as! ConfirmInfoTableViewCell
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PickDropTableViewCell", for: indexPath) as! PickDropTableViewCell
            return cell
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailNotifTableViewCell", for: indexPath) as! DailNotifTableViewCell
        cell.switchView.isEnable = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 300 : 90
    }
}
