//
//  DelieveryDetailsVC.swift
//  Autocrat
//
//  Created by Ios_Team on 17/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import MapKit
class DelieveryDetailsVC: RootVC<ConfirmInfoTableViewCell,IntroCollectionViewCell,[String],[String]> {

    var pointAnnotation:CustomPointAnnotation!
    let locationManager = CLLocationManager()
    var pinAnnotationView:MKPinAnnotationView!
    @IBOutlet weak var mapViews: MKMapView!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var footerViews:DelieveryFooterView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapViews.showsUserLocation = true
        self.mapViews.delegate = self
        
        //            let annotation = annotationViewArr[i]
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: 4.66455174, longitude: -74.07867091)
        mapViews.addAnnotation(annotation)
        footerViews.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.userImage.layer.cornerRadius = self.userImage.frame.height*0.5
    }
    @IBAction func btn_BackPres(_ sender: Any) {
        self.backToPreviousController()
    }
    
}

extension DelieveryDetailsVC: DelieveryFooterViewDelegate{
    func bottomOPtionClicked(index: Int) {
        self.selectedBottomOption = index
        self.jumpToBottomOption(index: index)
    }
}
extension DelieveryDetailsVC : MKMapViewDelegate, CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        self.mapViews.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
//        for i in 0..<lattitudeArr.count {
//            let center = CLLocationCoordinate2D(latitude: lattitudeArr[i], longitude: longitudeArr[i])
//            let regionO = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
//            self.mapViews.setRegion(regionO, animated: true)
//        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let lattitude = "\(annotation.coordinate.latitude)"
        let reuseIdentifier = "locationPin_\(lattitude)"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "locationPin.png")
        
        return annotationView
    }
}
