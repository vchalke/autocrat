//
//  LocateVehicleVC.swift
//  Autocrat
//
//  Created by Ios_Team on 15/12/20.
//  Copyright © 2020 Techathalon. All rights reserved.
//

import UIKit
import MapKit

class LocateVehicleVC: RootVC<ConfirmInfoTableViewCell,IntroCollectionViewCell,[String],[String]> {

    @IBOutlet weak var mapViews: MKMapView!
    @IBOutlet weak var tblViewss: UITableView!
    var annotationViewArr = [MKPointAnnotation]()
    var pointAnnotation:CustomPointAnnotation!
    let locationManager = CLLocationManager()
    var pinAnnotationView:MKPinAnnotationView!
    let lattitudeArr = [4.66455174, 6.24478548, 7.06125013, 7.88475514, 3.48835279]
    let longitudeArr = [-74.07867091, -75.57050110, -73.84928550, -72.49432589, -76.51532198]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initTableViewMultipleCell(tableView: self.tblViewss, baseController: self, arrCellId: ["ConfirmInfoTableViewCell","PickDropTableViewCell","StartFromTableViewCell","DailNotifTableViewCell"])
        
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapViews.showsUserLocation = true
        self.mapViews.delegate = self
        
        let annonOne = MKPointAnnotation()
        let annonTwo = MKPointAnnotation()
        let annonThree = MKPointAnnotation()
        let annonFour = MKPointAnnotation()
        let annonFive = MKPointAnnotation()
        
        
        
        annotationViewArr = [annonOne,annonTwo,annonThree,annonFour,annonFive]
        for i in 0..<lattitudeArr.count {
//            let annotation = annotationViewArr[i]
            let annotation = MKPointAnnotation()
//           annotation.coordinate = CLLocationCoordinate2D(latitude: lattitudeArr[i], longitude: longitudeArr[i])
            annotation.coordinate = CLLocationCoordinate2D(latitude: 11.21, longitude: 21.11)
           mapViews.addAnnotation(annotation)
        }
        
        
    }

    

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors " + error.localizedDescription)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.setNavigatiobToLeft()
        self.navigationController?.popViewController(animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmInfoTableViewCell", for: indexPath) as! ConfirmInfoTableViewCell
            return cell
        }else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "PickDropTableViewCell", for: indexPath) as! PickDropTableViewCell
            return cell
        }else if(indexPath.row == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "StartFromTableViewCell", for: indexPath) as! StartFromTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailNotifTableViewCell", for: indexPath) as! DailNotifTableViewCell
        cell.switchView.isEnable = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 300 : 90
    }
}

extension LocateVehicleVC : MKMapViewDelegate, CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        self.mapViews.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
//        for i in 0..<lattitudeArr.count {
//            let center = CLLocationCoordinate2D(latitude: lattitudeArr[i], longitude: longitudeArr[i])
//            let regionO = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
//            self.mapViews.setRegion(regionO, animated: true)
//        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let lattitude = "\(annotation.coordinate.latitude)"
        let reuseIdentifier = "locationPin_\(lattitude)"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "locationPin.png")
        
        return annotationView
    }
}



    


class CustomPointAnnotation: MKPointAnnotation {
var pinCustomImageName:String!
}
