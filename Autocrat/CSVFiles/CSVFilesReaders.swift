//
//  CSVFilesReaders.swift
//  Autocrat
//
//  Created by Ios_Team on 23/01/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import SwiftCSV

class CSVFilesReaders: NSObject {
    
    static func getHeaderArr(fileName:String)-> [String]? {
        do {
            let csvFile : CSV = try CSV(url: URL(fileURLWithPath: fileName))
            let headers = csvFile.header
            return headers
        } catch{
            printLog(error)
        }
        return nil
    }
    
    static func getArrayFromFile(fileName:String, key:String)-> [String]? {
        do {
            let csvFile : CSV = try CSV(url: URL(fileURLWithPath: fileName))
            let columnForKey = csvFile.namedColumns[key]
            return columnForKey
        } catch{
            printLog(error)
        }
        return nil
    }
    
}

/*


 func readCSVOne(){
         guard let csvPath = Bundle.main.path(forResource: "AddressLiscence_copy", ofType: "csv") else { return }
         do {
             let csvFile : CSV = try CSV(url: URL(fileURLWithPath: baseFilePath+FileName.ADDRESS_LISCENCE.rawValue+"_copy.csv"))
             // Rows
             let rows = csvFile.namedRows
             printLog(rows)
             let headers = csvFile.header  //=> ["id", "name", "age"]
             printLog(headers)
             let alice = csvFile.namedRows[0]    //=> ["id": "1", "name": "Alice", "age": "18"]
             printLog(alice)
             let bob = csvFile.namedRows[1]      //=> ["id": "2", "name": "Bob", "age": "19"]
             printLog(bob)
             // Columns
             let columns = csvFile.namedColumns
             printLog(columns)
             let names = csvFile.namedColumns["address"]  //=> ["Alice", "Bob", "Charlie"]
             printLog(names!)
             let ages = csvFile.namedColumns["age"]
             printLog(ages!)
             
         } catch{
             printLog(error)
         }
     }
 */
